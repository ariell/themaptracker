class Mission:
    def __init__(self):
        self.originLat = 999;
        self.originLon = 999;
        self.waypoints = [];

    def load(self,filename):
        # Load the mission file extracting the way points
        missionFile = open(filename)

        # skip through untile we find the word start
        missionSection = False

        for line in missionFile:
            if line == 'START\r\n':
                missionSection = True
            elif line == 'END\r\n':
                missionSection = False
            elif missionSection:
                missionLine = line.split(';')
                # Use the first point as the origin
                if self.originLat == 999:
                    self.originLat = missionLine[1]
                if self.originLon == 999:
                    self.originLon = missionLine[2]
                wpt = [float(missionLine[1]), float(missionLine[2])]
                self.waypoints.append(wpt)

    def dumpSimple(self):
        return self.waypoints

    def getOriginLat(self):
        return self.originLat

    def getOriginLon(self):
        return self.originLon

