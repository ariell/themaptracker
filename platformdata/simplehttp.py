######################################################################
# This file handles all the platform pose updates.
#
# There are some TODOS listed throughout the file with instructions.
# See documentation below for more info.
#
# Contact me if you have any questions.
#
# Author:
#   Ariell Friedman
#   ariell.friedman@gmail.com
#   25 AUG 2014
#
######################################################################

import time
from PlatformBase import PlatformBase


class Platform(PlatformBase):

    def __init__(self):
        PlatformBase.__init__(self)
        pass

    # override setdata to add a couple of fields
    def set_data(self, platform, data, field=None):
        # set mandatory fields
        PlatformBase.set_data(self, platform, {'state': 'online', 'statemsg': '', 'msgts': int(time.time())}, log=False)
        # set fields that are passed in
        PlatformBase.set_data(self, platform, data, field=field)



if __name__ == "__main__":
    pf = Platform()
    while True:
        print "--------------------"
        for p in pf.platformdata:
            print "***", p, ":\n",  pf.platformdata[p]
        time.sleep(1)
