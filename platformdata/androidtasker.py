######################################################################
# This file handles all the platform pose updates.
#
# There are some TODOS listed throughout the file with instructions.
# See documentation below for more info.
#
# Contact me if you have any questions.
#
# Author:
#   Ariell Friedman
#   ariell.friedman@gmail.com
#   25 AUG 2014
#
######################################################################

import time
from PlatformBase import PlatformBase


class Platform(PlatformBase):
    def __init__(self):
        PlatformBase.__init__(self)

    # override set_data to construct dictionary
    def set_data(self, platform, data, field=None):
        print data
        data = {
            'state': 'online',
            'msgts': int(time.time()),
            'pose': {
                'lat': round(float(data['pos'][0].split(',')[0]), 10),  # round(o[0]+(random()-0.5)/600, 12),  # REQUIRED (decimal degrees)
                'lon': round(float(data['pos'][0].split(',')[1]), 10),  # round(o[1]+(random()-0.5)/600, 12),  # REQUIRED (decimal degrees)
                'heading': 0,  # randint(0, 360)                  # REQUIRED (degrees)
                'uncertainty': float(data['uncertainty'][0]),
                'speed': float(data['speed'][0]),
                'altitude': float(data['altitude'][0])
            },
            'stat': {
                'bat': int(data['bat'][0]),
                'cellstren': data['cellstrength'][0],
                'uptime': data['uptime'][0],
                'gps': data['gps'][0]
            }
        }

        # Call super method with new data
        PlatformBase.set_data(self, platform, data)


if __name__ == "__main__":
    pf = Platform()
    while True:
        print "--------------------"
        for p in pf.platformdata:
            print "***", p, ":\n",  pf.platformdata[p]
        time.sleep(1)
