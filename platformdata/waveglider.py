import time
import threading
from PlatformBase import PlatformBase


# For waveglider
import pycurl
import cStringIO


class Platform(PlatformBase):
    def __init__(self):
        WaveGliderWGMSThread('WGMS.WGLIDER', self, 60).start()
        PlatformBase.__init__(self)


class WaveGliderWGMSThread (threading.Thread):
    #convert the following:
    #curl -k -H "Content-Type: text/xml; charset=utf-8" --dump-header headers -H "SOAPAction:" -d @Documents/waveglider/loginsoap.xml -X POST https://gliders.wgms.com/webservices/entityapi.asmx
    #curl -b headers "http://uh.wgms.com/pages/exportPage.aspx?viewid=36113&entitytype=42"
    def __init__(self, platform, platformobj, delay):
        threading.Thread.__init__(self)
        self.platform = platform
        self.platformobj = platformobj
        self.delay = delay
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit
        # authentication info
        self.auth_url = 'https://gliders.wgms.com/webservices/entityapi.asmx'
        self.auth_xml = '/home/oshirojk/Documents/waveglider/loginsoap.xml'
        self.oldmsgid = 0

        # data retrieve url
        self.export_url = 'http://uh.wgms.com/pages/exportPage.aspx?viewid=36113&entitytype=42'

    def run(self):

        while 1:
            try:
                # get data
                buf = cStringIO.StringIO()
                c = pycurl.Curl()
                c.setopt(pycurl.URL, self.export_url)
                c.setopt(pycurl.COOKIEFILE, 'datatools/waveglider_header')
                c.setopt(pycurl.WRITEFUNCTION, buf.write)
                c.setopt(pycurl.VERBOSE, True)
                c.perform()
                curldata = buf.getvalue()
                buf.close()

                print "---\n", curldata, "\n---"
                data = curldata.split('\n')[1].split(',')
                msgid = int(time.mktime(time.strptime(data[0], "%m/%d/%Y  %I:%M %p")))
                if self.oldmsgid < msgid:
                    self.oldmsgid = msgid
                    data = {
                        'state': 'online',
                        'msgts': int(time.time()),
                        'pose': {
                            'lat': float(data[11]),
                            'lon': float(data[12]),
                            'heading': float(data[3]),
                            'speed': round(float(data[1])*0.51, 1)
                        },
                        'stat': {
                            'bat': round(float(data[8])/6.6, 1),
                            'ftemp': int(data[6]),
                            'HDG': 'F:{}, S:{}'.format(data[5],data[4]),
                            'pressure': int(data[7])
                        }
                    }

                    self.platformobj.set_data(self.platform, data, )
            except:
                print "\n\n\n\nERROR!!! Unable to get WGMS data\n\n\n\n"

            time.sleep(self.delay)


if __name__ == "__main__":
    pf = Platform()
    while True:
        print "--------------------"
        for p in pf.platformdata:
            print "***", p, ":\n",  pf.platformdata[p]
        time.sleep(1)
