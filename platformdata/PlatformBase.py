import json
import os
import time
import collections
import inspect
from glob import glob
from shutil import copyfile
from datetime import datetime
import pandas as pd


class PlatformBase():

    def __init__(self):
        basedir = os.path.dirname(os.path.abspath(__file__))
        self.platformtrackhistory = {}
        self.datapath = basedir+"/logs/{campaign}/{action}/{data_type}/{platform}.json"
        self.campaign = None

    def set_campaign(self, campaign):
        self.campaign = campaign

    def get_data(self, platform, gethistory=False, setdata=None, field=None, campaign=None, replay_ts=None):
        """
        Get data for a specific platform

        :param campaign:
        :param platform:
        :param gethistory:
        :param setdata:
        :param field:
        :return:
        """
        # Set data if data is provided
        if setdata is not None:
            self.set_data(platform, setdata, field)

        # return empty if platform data is not initialised
        # if platform not in self.platformdata:
        #     return {}

        # Get data
        #data = self.platformdata[platform]      # get data
        data = None
        pose_history = None
        if replay_ts is None:  # if replay_ts is not set, get current log
            data_file = self.get_log_filename(platform, data_type="platform", campaign=campaign)
            curts = time.time()        # add current ts
            if gethistory and platform in self.platformtrackhistory:
                try:
                    pose_history = zip(list(self.platformtrackhistory[platform]['lat']), list(self.platformtrackhistory[platform]['lon']))
                except Exception as e:
                    print "Error getting platform history! ({})".format(e)
        else:                   # if replay, get closest matching log file from archive
            data_file = self.get_replay_log_file(platform, replay_ts, campaign, data_type="platform")
            curts = float(replay_ts)

        data = jsonfile(data_file)
        data['curts'] = curts        # add current ts
        data["pose_history"] = pose_history

        return data

    def get_replay_log_file(self, platform, replay_ts, campaign, data_type="platform"):
        data_file_pattern = self.get_log_filename(platform, data_type=data_type, archive_ts=replay_ts, return_pattern=True, archive=True, campaign=campaign)
        df_fname = os.path.join(os.path.dirname(data_file_pattern), "{}.index.pkl".format(platform))

        try:
            # If first enquiry, generate index, otherwise load from disk
            if os.path.isfile(df_fname):
                df = pd.read_pickle(df_fname)
            else:
                df = pd.DataFrame(columns=["src","platform","filepath"])
                for f in glob(data_file_pattern):
                    params = os.path.splitext(os.path.basename(f))[0].split("-")
                    ts = int(float(params[0]))
                    row = {"filepath": f}
                    df.at[ts, :] = row
                df.to_pickle(df_fname)

            data_file = df.iloc[df.index.get_loc(int(float(replay_ts)), method="pad")]["filepath"]
        except Exception as e:
            print("***ERROR getting match ()".format(e))
            data_file = "**NO-MATCH**"

        return data_file

    def set_data(self, platform, data, field=None, log=True, campaign=None):
        """

        :param platform:
        :param data:
        :return:
        """
        try:
            data['msgts'] = time.time()  # apply timestamp to data
        except Exception as e:
            print "*** ERROR: unable to set msgts: {}".format(e)

        if log:
            self.log_data(platform, data, campaign=campaign)

        jsonfile(self.get_log_filename(platform, data_type="platform", campaign=campaign), field=field, data=data)
        # if field is not None:   # set particular field
            # if platform not in self.platformdata:
            #     self.platformdata[platform] = {}
            # self.platformdata[platform][field] = data
        # else:                   # set all fields for platform
            # self.platformdata[platform] = data

    def log_data(self, platform, data, campaign=None):
        """

        :param platform:
        :return:
        """
        # TODO: replace deque variable with actual file system object or DB for threads
        if platform not in self.platformtrackhistory:
            self.platformtrackhistory[platform] = {
                'lat': collections.deque([], 100),
                'lon': collections.deque([], 100)
            }

        if 'pose' in data:
            self.platformtrackhistory[platform]['lat'].appendleft(data['pose']['lat'])
            self.platformtrackhistory[platform]['lon'].appendleft(data['pose']['lon'])

        # TODO: LOG DATA TO DB
        fname_archive = self.get_log_filename(platform, archive=True, campaign=campaign)
        fname_current = self.get_log_filename(platform, campaign=campaign)

        if not os.path.isdir(os.path.dirname(fname_archive)):
            os.makedirs(os.path.dirname(fname_archive))
        if os.path.isfile(fname_current):
            copyfile(fname_current, fname_archive)

    def parse_mission(self, platform, filepath, orig=frozenset([0, 0]), **kwargs):
        """
        Parse mission file
        This platform
        Outputs latlngs and origin from mission file
        TODO: parse actual mission file

        :param platform:
        :param filepath:
        :param orig:
        :return:
        """
        return self.get_platformmission(platform, **kwargs)

    def send_command(self, platform, args, getform=False):
        """
        Execute a command using name arguments contained in args
        args is a dict with string values (even number are returned as
        strings and need to be converted).

        :param platform:
        :param args:
        :param getform:
        :return:
        """
        print "\n\n\nTODO: OVERRIDE THIS METHOD\nPLATFORM:", platform, "\nARGS:", args, "\nGETFORM:", getform, "\n\n\n"
        return 0

    def get_platformmission(self, platform, campaign=None, replay_ts=None):
        """

        :param platform:
        :return:
        """
        return jsonfile(self.get_log_filename(platform, data_type="mission", campaign=campaign))
        # if platform not in self.platformmission:
        #     self.platformmission[platform] = None
        #
        # return self.platformmission[platform]

    def set_platformmission(self, platform, data, log=True, campaign=None):
        """

        :param log:
        :param platform:
        :param data:
        :return:
        """
        # Add required fields to payload
        try:
            data['msgts'] = int(time.time())  # apply timestamp to data
        except Exception as e:
            print ("*** ERROR: unable to set msgts: {}".format(e))

        # TODO: LOG DATA TO DB
        fname_current = self.get_log_filename(platform, data_type="mission", campaign=campaign)

        if log:
            fname_archive = self.get_log_filename(platform, data_type="mission", archive=True, campaign=campaign)
            if not os.path.isdir(os.path.dirname(fname_archive)):
                os.makedirs(os.path.dirname(fname_archive))
            if os.path.isfile(fname_current):
                copyfile(fname_current, fname_archive)

        jsonfile(fname_current, data=data)
        # self.platformmission[platform] = data

    def get_log_filename(self, platform, data_type="platform", archive=False, return_pattern=False, archive_ts=None, campaign=None):
        campaign = campaign or self.campaign
        src = os.path.splitext(os.path.basename(inspect.getfile(self.__class__)))[0]   # get name of file for module
        action = "archive" if archive else "current"
        plaform_fname = src+"-"+platform
        try:
            if archive:
                archive_ts = float(archive_ts) if archive_ts is not None else time.time()
                dt = datetime.fromtimestamp(archive_ts)
                file_ts = archive_ts if not return_pattern else "*"
                plaform_fname = "{dt.year}/{dt.month}/{dt.day}/{dt.hour}/{ts}-{platform}".format(dt=dt, ts=file_ts, platform=plaform_fname)
            return self.datapath.format(campaign=campaign, data_type=data_type, action=action, platform=plaform_fname)
        except Exception as e:
            print ("***ERROR obtaining file name ({})".format(e))
            return "**NO-MATCH**"


def jsonfile(filepath, field=None, data=None):
    # if data is provided, write to file
    if data is not None:
        if field is None:
            json_new = data
        else:
            json_new = jsonfile(filepath)
            json_new[field] = data

        # make dir if doesn't exist
        if not os.path.isdir(os.path.dirname(filepath)):
            os.makedirs(os.path.dirname(filepath))
        # write file
        open(filepath, 'w').write(json.dumps(json_new, indent=4))
        return data

    # read from file
    if os.path.isfile(filepath):
        with open(filepath) as config_json:
            if field is None:
                return json.load(config_json)
            else:
                return json.load(config_json).get(field, None)

    # return nothing if file does not exist
    else:
        return {} if field is None else None