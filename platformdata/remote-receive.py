######################################################################
# This file handles all the platform pose updates.
#
# There are some TODOS listed throughout the file with instructions.
# See documentation below for more info.
#
# Contact me if you have any questions.
#
# Author:
#   Ariell Friedman
#   ariell.friedman@gmail.com
#   25 AUG 2014
#
######################################################################

from random import random, randint
import time
import threading
import math
import json
import requests
import os

# This global dictionary stores all the platform information updates
platformdata = {}

######################################################################
# Start threads for platform updates
# These are currently fake threads that update the vehicle
# poses. TODO: Make them real or replace them with something similar!
######################################################################
def init():
    return


def init_push_data(configfile):
    return

######################################################################
# Get data for a specific platform
# The global variable platformdata is updated by another process/thread.
# This function simply reads the output for a specific platform
######################################################################
def get_data(platform):
    global platformdata
    data = platformdata[platform]  # get data
    data['curts'] = int(time.time())    # gets curts from remote post
    #if (data['curts']-data['msgts']) > 30:
    #    data['pose']['uncertainty'] = (data['curts']-data['msgts'])*0.5
    return data


def set_platformdata(platform=None, data={}):
    global platformdata
    if platform is None:
        platformdata = json.loads(data)
        dirname = "logs/{}".format(time.strftime("%Y/%m/%d"))
        filename = "{}/{}-{}.json".format(dirname, os.path.basename(__file__), int(time.time()))
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        f = open(filename, 'w')
        f.write(data)
        f.close()
    else:
        platformdata[platform] = json.loads(data)
    return



######################################################################
# Parse mission file
# This platform
# Outputs latlngs and origin from mission file
# TODO: parse actual mission file
######################################################################
def parse_mission (filepath, origin=[0, 0]):
    # TODO: parse mission file and origin and return in LAT/LON
    # filepath will contain the filepath to the mission file
    url = "{}/get_mission?filepath={}&olat={}&olon={}".format(sourceserver[0],filepath, origin[0], origin[1])
    data = json.loads(requests.get(url=url).text)
    #print data
    latlngs = data["latlngs"]
    origin = data["origin"]

    return latlngs, origin

