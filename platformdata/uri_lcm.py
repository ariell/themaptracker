######################################################################
# This file handles all the platform pose updates.
#
# There are some TODOS listed throughout the file with instructions.
# See documentation below for more info.
#
# Contact me if you have any questions.
#
# Author:
#   Ariell Friedman
#   ariell.friedman@gmail.com
#   31 MAY 2016
#
######################################################################

import time
import threading
import sys
import select
import pickle
import collections
from random import random
from geojson import Point, FeatureCollection, LineString, Feature, Polygon
from flask import Flask, render_template
from PlatformBase import PlatformBase


#########################################
# START: EDIT_THIS
# TODO: MODIFY THIS TO MAKE IT CORRECT
#########################################
import lcm
sys.path.append("/home/croman/research/git_code/field/src/platforms/kayak/build/libraries/uriocelcmtypes/lcmtypes/python")
from urioce_lcm import kayak_status_t, string_label_value_t, string_label_value_N_t # import the right module
KAYAK_STAT = "KAYAK_STATUS"             # name of message to subscribe to
KAYAK_MISSION = "MAPTRACKER"
#########################################
# END: EDIT_THIS
#########################################


######################################################################
# Inherit properties and methods from PlatformBase class
######################################################################
class Platform(PlatformBase):

    log_messages = collections.deque([], 100)
    heatmap_depth = collections.deque([], 1000)

    def __init__(self):
        PlatformBase.__init__(self)
        self.lc = lcm.LCM()
        LcmThread(self,self.lc).start()

    # override parse_mission in PlatformBase
    def parse_mission(self, platform, filepath, orig=None):
        platformmission = self.get_platformmission(platform)
        if platformmission is not None:
            mission = platformmission
        else:
            mission = {}   # if no mission set, return empty object. GUI will keep trying until it finds a mission to load.

        return mission

    def send_command(self, platform, args, getform=False):
        """
        Execute a command using name arguments contained in args
        args is a dict with string values (even number are returned as
        strings and need to be converted).

        :param platform: the name of the platform
        :param args: dict parameters containing string values (even number are returned as
                     strings and need to be converted).
        :param getform: this is handled by the server. If true, it will return the HTML form, otherwise, it attempts
                        to execute the command.
        :return: int, 0 if success, anything else is error.
        """
        if getform:  # if getform is true, return the html form that sends the command
            return render_template('html-snippets/uri-kayak-command.html', platform=platform)
        else:        # otherwise, execute the command
            #TODO: do something useful with the platform and args to send the command(s) to the KAYAK.
            if args['command'] == 'goto':
                # This is a GOTO command, currently just printing stuff, should do something more useful.
                print "THIS IS A GOTO COMMAND"
                msg = string_label_value_N_t()
                msg.msg_type = 'goto'
                msg.num_fields = 4
                msg.fields.append(string_label_value_t())
                msg.fields[0].label = 'lat'
                msg.fields[0].value = args["lat"]
                msg.fields.append(string_label_value_t())
                msg.fields[1].label = 'lon'
                msg.fields[1].value = args["lon"]
                msg.fields.append(string_label_value_t())
                msg.fields[2].label = 'timeout'
                msg.fields[2].value = args["timeout"]
                msg.fields.append(string_label_value_t())
                msg.fields[3].label = 'speed'
                msg.fields[3].value = args["speed"]
                #Publish a message back to the kayak mission exec
                self.lc.publish("GUI_COMMAND", msg.encode())

            elif args['command'] == 'abort':
                print "THIS IS AN ABORT COMMAND"
                msg = string_label_value_N_t()
                msg.msg_type = 'abort'
                msg.num_fields = 1 
                msg.fields.append(string_label_value_t())
                msg.fields[0].label = 'abort'
                msg.fields[0].value = '0'
                #Publish a message back to the kayak mission exec
                self.lc.publish("GUI_COMMAND", msg.encode())
            
            elif args['command'] == 'home':
                print "THIS IS AN HOME COMMAND"
                msg = string_label_value_N_t()
                msg.msg_type = 'home'
                msg.num_fields = 1 
                msg.fields.append(string_label_value_t())
                msg.fields[0].label = 'home'
                msg.fields[0].value = '0'
                #Publish a message back to the kayak mission exec
                self.lc.publish("GUI_COMMAND", msg.encode())

            else: 
                return 1  # command not recognised

        return 0

    # Added heatmap method. This has been added to config file.
    def get_depth_heatmap(self, npoints=100):
        data = list(self.heatmap_depth)  # get the heatmap for the chosen sensor
        # get the max value to set heatmap max colour
        max_value = max([i['value'] for i in data])
        # this is the format for the heatmap layer
        return {"max": max_value, "data": data}

    def get_log(self, timestamp=0):
        # Only return messages that are after timestamp
        messages = [m for m in list(self.log_messages) if m[0] > float(timestamp)]
        return {'messages': messages, 'timestamp': time.time()}

    def log_msg(self, msg, type):
        self.log_messages.append([time.time(), type, msg])

######################################################################
#
######################################################################

  #int64_t	timestamp;
  #boolean	mis_active;
  #int8_t	mis_status;
  #int64_t	mis_mistime;
  #int64_t	mis_legtime;
  #int32_t	mis_legnum;
  #int32_t	mis_segnum;
  #int8_t	mis_legtype;
  #int8_t	mis_ctrl_mode; 
  #int8_t	ctrl_status;
  #float	ctrl_thrust; // thruster percent power
  #float	ctrl_rudder; // rudder position, -100 to 100
  #int8_t       utm_zone; // UTM zone number, 6 deg method
  #double	ctrl_northing; // meters north of equator
  #double	ctrl_easting; // meters east of utm_zone zero
  #double	ctrl_lat; // decimal degrees
  #double	ctrl_lon; // decimal degrees
  #float		ctrl_speed; // speed over water, knots
  #float		ctrl_heading; // degrees true
  #float		ctrl_sog; // speed over ground, knots
  #float		ctrl_cmg; // degrees true
  #float		ctrl_age; // seconds since last controller message
  #boolean	radio_incontrol; // true if radio is overriding autonomous control
  #float		radio_thrust;
  #float		radio_rudder;
  #float		bat_voltage; // battery voltage
  #float		bat_current; // current draw, amps
  #float		bat_charge; // percent battery charge, 0-100
  #float		bat_age; // seconds since last battery message
  #int8_t	num_sensors;
  #sensor_data_t	sensor_data[num_sensors];

class LcmThread(threading.Thread):

    def __init__(self, platformobj, lc):
        threading.Thread.__init__(self)
        self.platformobj = platformobj
        self.lc = lc
        self.exitFlag = False
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit

        # self.lat_vector = collections.deque(maxlen=1000)
        # self.lon_vector = collections.deque(maxlen=1000)
        # self.depth_vector = collections.deque(maxlen=1000)

        self.new_depth = False
        
    def KayakStatusHandler(self, channel, data):
        print channel
        msg = kayak_status_t.decode(data)
        platform = "KAYAK1" #channel #'auv{}'.format(msg.target_id)


        #handle sensor data within the message
        if msg.num_sensors > 0: 
            for ii in range(msg.num_sensors):
                if msg.sensor_data[ii].name == 'Depth':
                    depth = msg.sensor_data[ii].value
                    depth_time = msg.sensor_data[ii].timestamp
                    # self.depth_vector.append(depth)
                    self.new_depth = True
        else: 
            depth = 0

        #put position into vector
        if(self.new_depth == True):
            # LOG MESSAGE
            self.platformobj.log_msg("Adding new depth to depth heatmap!", "info")
            self.platformobj.depth_heatmap.append({"lat": round(msg.ctrl_lat, 6), "lng": round(msg.ctrl_lat, 6), "value":depth})
            # self.lat_vector.append(round(msg.ctrl_lat, 6))
            # self.lon_vector.append(round(msg.ctrl_lat, 6))
            self.new_depth = False
            
            
        #ctrl_mode; // 0 idle, 1 oloop, 2 heading, 3 goto, 4 hold, 5 line follow
        ctrl_mode = ['Idle', 'oloop', 'Heading', 'Goto', 'Hold', 'Line follow']

        # handle alerts
        radio = ("radio:on", 1) if msg.radio_incontrol else ("radio:off", 0)

        # set platform data
        data = {
            'state': 'online',
            'msgts': int(time.time()),
            'pose': {
                'lat': round(msg.ctrl_lat, 6),                 # REQUIRED (decimal degrees)
                'lon': round(msg.ctrl_lon, 6),                 # REQUIRED (decimal degrees)
                'heading': round(msg.ctrl_heading,1),          # REQUIRED (degrees)
                'speed': round(msg.ctrl_speed*0.514444, 2),    # speed (m/s)
                'depth': round(depth,2) #depth (m)
                #'uncertainty': 1
            },
            'stat': {
                # You can add whatever custom status messages you like here
                # NB: 'bat' has a special display widget (range 0-100)
                # Any others, just get displayed normally
                'bat_voltage': round(msg.bat_voltage,2),
                'bat_current': round(msg.bat_current,2),
                'bat_charge': round(msg.bat_charge,2),
                'bat_age': round(msg.bat_age,2),
                'rudder': round(msg.ctrl_rudder, 2),
                'thrust': round(msg.ctrl_thrust, 2),
                'mode': ctrl_mode[msg.mis_ctrl_mode]
            },
            'alert': {
                # You can add whatever custom alerts you like here.
                # Must return 1 for error, 0, for OK.
                'Pump' : msg.pump_state,
              }
        }

        self.platformobj.set_data(platform, data, )

    def KayakMissionHandler(self, channel, data):
        try: msg = string_label_value_t.decode(data)
        except ValueError: 
            print("invalid json_mission message")
            return

        if msg.label == "mission_json":
            json_mission = pickle.loads(msg.value)
            json_mission['msgts'] = int(time.time())  # add timestamp for updates
            platform = "KAYAK1"
            print("Got new mission")
            self.platformobj.set_platformmission(platform, json_mission)

    def run(self):
        #self.lc = lcm.LCM()
        self.lc.subscribe(KAYAK_STAT, self.KayakStatusHandler)
        self.lc.subscribe(KAYAK_MISSION, self.KayakMissionHandler)

        timeout = 1  # amount of time to wait, in seconds
        while not self.exitFlag:
            rfds, wfds, efds = select.select([self.lc.fileno()], [], [], timeout)
            if rfds:
                self.lc.handle()


    
if __name__ == "__main__":
    pf = Platform()
    while True:
        print "--------------------"
        for p in pf.platformdata:
            print "***", p, ":\n",  pf.platformdata[p]
        time.sleep(1)
