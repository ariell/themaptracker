import collections
import time

import requests
from PlatformBase import PlatformBase
from flask import render_template


class Platform(PlatformBase):

    def __init__(self):
        PlatformBase.__init__(self)
        self.heatmaps = {}
        self.log_messages = collections.deque([], 100)

    # override setdata to add a couple of fields
    def set_data(self, platform, data, field=None, data_type="platform", campaign=None):
        if data_type == "platform":

            # Add required fields to payload
            if 'state' not in data:                             # add state if not provided
                data['state'] = 'online'
            if 'statemsg' not in data:                          # add state message if not provided
                data['statemsg'] = ''

            # Validate data and raise errors if necessary
            try:
                if data['state'] == 'online':
                    validate_platform_data(data)
            except ValueError as e:  # catch the value error, log it to the GUI and then re-raise the error
                self.log_msg(platform+": ERROR UPDATING PLATFORM DATA ("+str(e)+")", "danger")
                raise ValueError(str(e))

            # Set data
            PlatformBase.set_data(self, platform, data, field=field, campaign=campaign)

        elif data_type == "mission":

            # Validate data and raise errors if necessary
            validate_geojson(data)

            # Set data
            self.set_platformmission(platform, data, campaign=campaign)

        elif data_type == "log":

            # Validate data and raise errors if necessary
            validate_log(data)

            # Set data
            self.log_msg(platform+": "+data["msg"], data["type"])

        elif data_type == "sensor":
            # Set data  platform, sensor, data
            self.add_to_heatmap(platform, data["key"], data["value"])

        else:
            raise ValueError("Invalid data_type value. Must be [platform|mission|log]")

    def get_log(self, timestamp=0):
        # Only return messages that are after timestamp
        messages = [m for m in list(self.log_messages) if m[0] > float(timestamp)]
        return {'messages': messages, 'timestamp': time.time()}

    def log_msg(self, msg, msgtype):
        self.log_messages.append([time.time(), msgtype, msg])

    def send_command(self, platform, args, getform=False):
        server_url = "http://{}:{}/execute/{}".format(args.get("ip"), args.get("port"), platform)
        try:
            if getform:     # if getform is true, return the html form that sends the command
                print "\n\n*** GET: ", server_url, "\n\n", args, "\n\n"
                r = requests.get(server_url, params=args, timeout=10)
            else:           # otherwise, execute the command as a post
                print "\n\n*** POST: ", server_url, "\n\n", args, "\n\n"
                r = requests.post(server_url, data=args, timeout=20)
            return r.text, r.status_code
        except Exception as e:
            return str(e), 500

    def get_heatmap(self, sensor="temp", platform="", npoints=50):
        if platform in self.heatmaps and sensor in self.heatmaps.get(platform, {}):
            data = list(self.heatmaps[platform][sensor])  # get the heatmap for the chosen sensor
            # get the max value to set heatmap max colour
            max_value = max([i['value'] for i in data])
        else:
            data = []
            max_value = 0
        # this is the format for the heatmap layer
        return {"max": max_value, "data": data}

    def add_to_heatmap(self, platform, sensor, data):
        if platform not in self.heatmaps:
            self.heatmaps[platform] = {}
        if sensor not in self.heatmaps[platform]:
            self.heatmaps[platform][sensor] = collections.deque([], 200)  # initialise collection with 200 points
        self.heatmaps[platform][sensor].append(data)


def validate_geojson(data):
    # TODO: validate geojson object
    pass


def validate_log(data):
    check_field(data, "type", val_type=basestring, val_inlist=["success", "warning", "danger", "info"], required=True)
    check_field(data, "msg", val_type=basestring, required=True)


def validate_platform_data(data):
    # Validate ALERTS ##################
    check_field(data, "alert", val_type=dict, required=True)
    for k in data["alert"].keys():
        check_field(data["alert"], k, val_type=int, val_min=0, val_max=1)

    # Validate POSE ####################
    # required
    check_field(data, "pose", val_type=dict, required=True)
    check_field(data["pose"], "lat", val_type=float, val_min=-90, val_max=90, required=True)
    check_field(data["pose"], "lon", val_type=float, val_min=-180, val_max=180, required=True)
    # optional, but required for dash display widget
    check_field(data["pose"], "heading", val_type=float, val_min=0, val_max=360, required=False)
    check_field(data["pose"], "pitch", val_type=float, val_min=-180, val_max=180, required=False)
    check_field(data["pose"], "roll", val_type=float, val_min=-180, val_max=180, required=False)
    # optional
    check_field(data["pose"], "speed", val_type=float, val_min=0, required=False)
    check_field(data["pose"], "uncertainty", val_type=float, val_min=0, required=False)
    check_field(data["pose"], "alt", val_type=float, required=False)
    check_field(data["pose"], "depth", val_type=float, required=False)

    # Validate STATS ###################
    check_field(data, "stat", val_type=dict, required=True)
    check_field(data["stat"], "bat", val_type=int, val_min=0, val_max=100, required=False)

    # Validate STATE ###################
    check_field(data, "state", val_type=basestring, val_inlist=["online", "stationary", "follow"], required=True)


def check_field(data, key, val_type=None, val_min=None, val_max=None, val_inlist=None, required=True):
    # If key doesn't exist, return whether required or not
    if key in data:
        if val_type is not None:
            if not isinstance(data[key], val_type):
                raise ValueError("'{}' must be {}.".format(key, val_type))
        if val_min is not None:
            if data[key] < val_min:
                raise ValueError("'{}' can not be less than {}.".format(key, val_min))
        if val_max is not None:
            if data[key] > val_max:
                raise ValueError("'{}' can not be more than {}.".format(key, val_max))
        if val_inlist is not None:
            if data[key] not in val_inlist:
                raise ValueError("'{}' needs to be one of {}.".format(key, val_inlist))
    else:
        if required:
            raise ValueError("'{}' is missing.".format(key))
