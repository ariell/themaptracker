######################################################################
# This file handles all the platform pose updates.
#
# There are some TODOS listed throughout the file with instructions.
# See documentation below for more info.
#
# Contact me if you have any questions.
#
# Author:
#   Ariell Friedman
#   ariell.friedman@gmail.com
#   25 AUG 2014
#
######################################################################

import time
import threading
import sys
import math
import select
import os
import collections

import pyproj
from flask import Flask, render_template
from PlatformBase import PlatformBase

import lcm
from datatools import missionUVC


LCMROOT='/home/auv/git/acfr_lcm'
sys.path.append('{0}/build/lib/python{1}.{2}/dist-packages/perls/lcmtypes/'.format(LCMROOT, sys.version_info[0], sys.version_info[1]))


# TODO: remove try-except statement
try:
    from datatools import missionMP
except:
    print "Unable to load missionMP (Sirius)"
try:
    import missionXML
except:
    print "Unable to load missionXML (Iver)"


from acfrlcm import auv_status_short_t, ship_status_t
from senlcm import usbl_fix_t, uvc_osi_t
from acfrlcm.auv_global_planner_t import auv_global_planner_t
#except:
#    pass


class Platform(PlatformBase):

    platformmission = {}

    def __init__(self):
        LcmThread(self).start()

    def send_command(self, platform, args, getform=False):
        """

        :param platform:
        :param args:
        :param getform:
        :return:
        """

        # {'auvcmd': u'goto', 'auvcmdplatform': u'AUVSTAT.SIRIUS', 'auvcmdlon': u'121.86512589454651', 'auvcmdlat': u'-14.112437260470577'}
        if getform:
            return render_template('platforms/send-command-acfrauv.html', platform=platform)
        else:
            print args

            cmd = str(args['command'])
            lc = lcm.LCM()
            if cmd == "goto":
                msg = auv_global_planner_t()
                msg.point2_x = float(args['lon'])
                msg.point2_y = float(args['lat'])
                msg.point2_z = float(args['depth'])
                msg.point2_att[2] = float(-98765)
                msg.velocity[0] = float(args['velocity'])
                msg.timeout = float(args['timeout'])
                msg.command = auv_global_planner_t.GOTO

            elif cmd == "abort":
                msg = auv_global_planner_t()
                msg.command = auv_global_planner_t.ABORT
                msg.str = "MANUAL"
                #lc.publish('TASK_PLANNER_COMMAND.{}'.format(platform), msg.encode())
            else:
                return 1  # error

            lc.publish('TASK_PLANNER_COMMAND.{}'.format(platform), msg.encode())

        return 0

    ######################################################################
    #
    # TODO: parse actual mission file
    ######################################################################
        # override parse_mission in PlatformBase
    def parse_mission(self, platform, filepath, orig=None):
        origin = self.origin[::-1]   # NOTE: GeoJSON has different ordering of LAT/LONs

        if platform in self.platformmission:
            mission = mission_to_geojson(filepath, self.platformmission[platform], origin)
        else:
            mission = {}   # if no mission set, return empty object. GUI will keep trying until it finds a mission to load.

        return mission




######################################################################
#
######################################################################

class LcmThread(threading.Thread):

    def __init__(self, platformobj):
        threading.Thread.__init__(self)
        self.platformobj = platformobj
        self.exitFlag = False
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit


    def usblFixHandler(self, channel, data):
        msg = usbl_fix_t.decode(data)
        # msgid = msg.utime
        platform = channel  # 'usbl{}'.format(msg.remote_id)


        # double target_x;
        # double target_y;
        # double target_z;
        # double latitude;
        # double longitude;
        # double depth;
        # double accuracy;
        #
        # double ship_latitude;
        # double ship_longitude;
        # float ship_roll;
        # float ship_pitch;
        # float ship_heading;
        lat = round(math.degrees(msg.latitude), 8)
        lon = round(math.degrees(msg.longitude), 8)

        #platformtrackhistory[platform]['lat'].appendleft(lat)
        #platformtrackhistory[platform]['lon'].appendleft(lon)

        data = {
            # 'msgid': msgid,                                 # REQUIRED (number)
            'state': 'online',
            'msgts': int(time.time()),
            'pose': {
                'lat': lat,          # REQUIRED (decimal degrees)
                'lon': lon,          # REQUIRED (decimal degrees)
                'depth': round(msg.depth, 1),
                'XYZ': "{}, {}, {}".format(round(msg.target_x, 1), round(msg.target_y, 1), round(msg.target_z, 1)),
                'uncertainty': round(msg.accuracy, 2),
                'speed': 0.5
            }
        }

        self.platformobj.set_data(platform, data, )



    def shipStatusHandler(self, channel, data):
        msg = ship_status_t.decode(data)
        # msgid = msg.utime
        platform = channel;


        lat = round(math.degrees(msg.latitude), 8)
        lon = round(math.degrees(msg.longitude), 8)

        #platformtrackhistory[platform]['lat'].appendleft(lat)
        #platformtrackhistory[platform]['lon'].appendleft(lon)

        data = {
            # 'msgid': msgid,                                 # REQUIRED (number)
            'state': 'online',
            'msgts': int(time.time()),
            'pose': {
                'lat': lat,          # REQUIRED (decimal degrees)
                'lon': lon,         # REQUIRED (decimal degrees)
                'heading': round(math.degrees(msg.heading), 2),        # REQUIRED (degrees)
                'roll': round(math.degrees(msg.roll), 2),        # REQUIRED (degrees)
                'pitch': round(math.degrees(msg.pitch), 2),        # REQUIRED (degrees)
                'speed': 1
            }
        }

        self.platformobj.set_data(platform, data, )




    def auvStatusHandler(self, channel, data):
        #print channel
        msg = auv_status_short_t.decode(data)
        #print msg
        platform = channel #'auv{}'.format(msg.target_id)


        lat = round(math.degrees(msg.latitude), 8)
        lon = round(math.degrees(msg.longitude), 8)

        # msgid = msg.utime
        data = {
            # 'msgid': msgid,                                 # REQUIRED (number)
            'state': 'online',
            'msgts': int(time.time()),
            'pose': {
                'lat': lat,                  # REQUIRED (decimal degrees)
                'lon': lon,                 # REQUIRED (decimal degrees)
                'heading': round(msg.heading/10.0,1), # REQUIRED (degrees)
                'alt': float(msg.altitude)/10.0,                           # OPTIONAL (m)
                'depth': float(msg.depth)/10.0,                            # OPTIONAL (m)
                'roll': round(msg.roll/10.0,1),       # OPTIONAL / REQUIRED for dashboard (degrees)
                'pitch': round(msg.pitch/10.0,1),      # OPTIONAL / REQUIRED for dashboard (degrees)
                'speed': 0.5
                #'uncertainty': 1
            },
            'stat': {
                # You can add whatever custom status messages you like here
                # NB: 'bat' has a special display widget
                # Any others, just get displayed normally
                'bat': msg.charge,                     # REQUIRED for dashboard (int 0-100)
                'waypt': msg.waypoint,
                '#imgs': msg.img_count
                #'state': 'OK'
            },
            'alert': {
                # You can add whatever custom alerts you like here.
                # Must return 1 for error, 0, for OK.
                'dvl':  msg.status & (1 << 0),
                'dvl_bl':  msg.status & (1 << 1),
                'gps':  msg.status & (1 << 2),
                'depth':  msg.status & (1 << 3),
                'comp':  msg.status & (1 << 4),
                'imu':  msg.status & (1 << 5),
                'oas':  msg.status & (1 << 6),
                'nav':  msg.status & (1 << 7),
                'epuck':  msg.status & (1 << 8),
                'abort':  msg.status & (1 << 9),
                'press':  msg.status & (1 << 13),
                'leak':  msg.status & (1 << 15)
            }
        }

        # Alert if AUV > 6 m
        if data['pose']['depth'] < 6:
            data['alert']['DEP<6'] = 1

        self.platformobj.set_data(platform, data, )

            
    def uvcOsiHandler(self, channel, data):
        msg = uvc_osi_t.decode(data)
        # msgid = msg.utime
        platform = channel  # 'usbl{}'.format(msg.remote_id)

        lat = round(math.degrees(msg.latitude), 8)
        lon = round(math.degrees(msg.longitude), 8)
        data = {
            # 'msgid': msgid,                                 # REQUIRED (number)
            'state': 'online',
            'msgts': int(time.time()),
            'pose': {
                'lat': lat,          # REQUIRED (decimal degrees)
                'lon': lon,         # REQUIRED (decimal degrees)
            }
        }

        self.platformobj.set_data(platform, data, )
        

    def run(self):
        self.lc = lcm.LCM()
        self.lc.subscribe("AUVSTAT.*", self.auvStatusHandler)
        self.lc.subscribe("SHIP_STATUS.*", self.shipStatusHandler)
        self.lc.subscribe("USBL_FIX.*", self.usblFixHandler)
        self.lc.subscribe("UVC_OSI", self.uvcOsiHandler)

        timeout = 1  # amount of time to wait, in seconds
        while not self.exitFlag:
            rfds, wfds, efds = select.select([self.lc.fileno()], [], [], timeout)
            if rfds:
                self.lc.handle()


# class sendRemoteDataThread (threading.Thread):
#     def __init__(self, delay, targets, destserver):
#         threading.Thread.__init__(self)
#         self.delay = delay
#         self.targets = targets
#         self.destserver = destserver
#         self.daemon = True  # run in daemon mode to allow for ctrl+C exit
#
#     def run(self):
#
#         while(1) :
#             sendplatforms = {}
#             for key in self.targets:
#                 key = key.strip()
#                 try:
#                     print "Getting {}".format(key)
#                     sendplatforms[key] = get_data(key)
#
#                 except:
#                     print "ERROR!!!   Unable to read {}".format(key)
#
#             try:
#                 print "Sending data to {}".format(self.destserver)
#                 payload = {'platformdata': json.dumps(sendplatforms)}
#                 r = requests.post(self.destserver, data=payload)
#             except:
#                 print "ERROR!!!   Unable to send data to {}".format(self.destserver)
#
#             time.sleep(self.delay)


def mission_to_geojson(filepath, platformmission, cfgorigin):

    try:
        #mission_file = "{}/{}".format(filepath, platformmission)

        fileName, fileExtension = os.path.splitext(filepath)
        if fileExtension.lower() == '.xml':
            mission = missionXML.Mission()
            startind = 0
        elif fileExtension.lower() == '.mp':
            mission = missionMP.Mission()
            startind = 2
        elif fileExtension.lower() == '.mis':
            mission = missionUVC.Mission()
        else:
            print 'Incorrect mission type'
            return

        if os.path.exists(filepath):
            mission.load(filepath)
        else:
            print "Mission does not exist."
            return

        origin = [mission.getOriginLat(), mission.getOriginLon()]
        if origin[0] == 0 or origin[1] == 0:
            origin = cfgorigin



        # UVC missions are already in Lat / Long
        if fileExtension.lower() == '.mis':
            latlngs = mission.dumpSimple()
        else:
            waypoints = mission.dumpSimple()
            # convert the waypoints to latitude and longitude
            projStr = '+proj=tmerc +lon_0={} +lat_0={} +units=m'.format(origin[1], origin[0])
            p = pyproj.Proj(projStr)

            latlngs = []
            for wpt in waypoints:
                ll = p(wpt.y, wpt.x, inverse=True)
                latlng = [ll[1], ll[0]]
                latlngs.append(latlng)

            latlngs = latlngs[startind:]

        mission = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "geometry": {"type": "Point", "coordinates": origin},
                    "properties": {
                        "options": {"radius": 6, "fillColor": "green", "color": "black", "weight": 2, "opacity": 0.5, "fillOpacity": 0.5},
                        "info": {"Type": "Point: Origin", "Name": platformmission}
                    }
                },
                {
                    "type": "Feature",
                    "geometry": {"type": "Point", "coordinates": latlngs[0]},
                    "properties": {
                        "options": {"radius": 6, "fillColor": "green", "color": "white", "weight": 1, "opacity": 1, "fillOpacity": 0.8},
                        "info": {"Type": "Point: START", "Name": platformmission}
                    }
                },
                {
                    "type": "Feature",
                    "geometry": {"type": "Point", "coordinates": latlngs[-1]},
                    "properties": {
                        "options": {"radius": 6, "fillColor": "green", "color": "black", "weight": 1, "opacity": 1, "fillOpacity": 0.8},
                        "info": {"Type": "Point: END", "Name": platformmission}
                    }
                },
                {
                    "type": "Feature",
                    "geometry": {"type": "LineString", "coordinates": latlngs},
                    "properties": {
                        "options": {"color": "green", "weight": 3, "opacity": 0.5, "dashArray": "5,5"},
                        "info": {"Type": "LineString: Path", "Name": platformmission, "Duration": "5 Hours"}
                    }
                }
            ]
        }

        return mission
    except:
        print "Unable to parse mission!!!"
        return {}


if __name__ == "__main__":
    pf = Platform()
    while True:
        print "--------------------"
        for p in pf.platformdata:
            print "***", p, ":\n",  pf.platformdata[p]
        time.sleep(1)