######################################################################
# This file handles all the platform pose updates.
#
# There are some TODOS listed throughout the file with instructions.
# See documentation below for more info.
#
# Contact me if you have any questions.
#
# Author:
#   Ariell Friedman
#   ariell.friedman@gmail.com
#   31 MAY 2016
#
######################################################################

from random import random, randint, randrange
import time
import threading
import math
from flask import Flask, render_template
from geojson import Point, FeatureCollection, LineString, Feature, Polygon
from PlatformBase import PlatformBase
import collections


class Platform(PlatformBase):

    #origin = [-33.840721, 151.254373]   # chowder bay
    #origin = [21.265672, -157.823995]    # waikiki aquarium
    origin = [20.72404658, -156.66]   # default origin AU-AU channel, hawaii
    heatmaps = {}
    log_messages = collections.deque([], 100)


    def __init__(self):
        PlatformBase.__init__(self)
        FakeAUVThread('AUV2', self, 0.5).start()
        FakeAUVThread('AUV1', self, 60).start()
        FakeShipThread('SHIP1', self, 1).start()
        FakeShipThread('USBL_FIX.AUV1', self, 5).start()

    ######################################################################
    # Parse mission file
    ######################################################################
    def parse_mission(self, platform, filepath, orig=None, **kwargs):
        # TODO: parse mission file and origin and return in LAT/LON
        # filepath will contain the filepath to the mission file

        origin = self.origin[::-1]   # NOTE: GeoJSON has different ordering of LAT/LONs

        platformmission = self.get_platformmission(platform, **kwargs)
        if platformmission is not None:
            mission = mission_to_geojson(filepath, platformmission, origin)
        else:
            mission = {}

        return mission

    ######################################################################
    # Execute a command using name arguments contained in args
    # args is a dict with string values (even number are returned as
    # strings and need to be converted).
    ######################################################################
    def send_command(self, platform, args, getform=False):
        if getform:  # if getform is true, return the html form that sends the command
            return render_template('platforms/send-command-demo.html', platform=platform)
        else:        # otherwise, execute the command
            print "\n\n\n-----------\n", platform, ":\n", args, "\n-----------\n\n\n"

        return 0

    ######################################################################
    #  EXAMPLES OF CUSTOM METHODS
    ######################################################################
    def random_logger(self, timestamp=0):
        # Only return messages that are after timestamp
        messages = [m for m in list(self.log_messages) if m[0] > float(timestamp)]
        return {'messages': messages, 'timestamp': time.time()}

    def log_msg(self, msg, type):
        self.log_messages.append([time.time(), type, msg])

    def random_heatmap(self, sensor="temp", platform="", npoints=50):
        data = list(self.heatmaps[platform][sensor])  # get the heatmap for the chosen sensor
        # get the max value to set heatmap max colour
        max_value = max([i['value'] for i in data])
        # this is the format for the heatmap layer
        return {"max": max_value, "data": data}

    def add_to_heatmap(self, platform, sensor, data):
        if platform not in self.heatmaps:
            self.heatmaps[platform] = {}
        if sensor not in self.heatmaps[platform]:
            self.heatmaps[platform][sensor] = collections.deque([], 200)  # initialise collection with 200 points
        self.heatmaps[platform][sensor].append(data)

######################################################################
# The thread classes below spoof fake random nav data for fake platforms
# It provides an example of the data structures that are required to feed real updates to the map
# TODO: create a similar class that fills out the data structure with real data
# It may be cleaner to keep the class another file that is imported into this one
class FakeAUVThread (threading.Thread):
    def __init__(self, platform, platformobj, delay):
        threading.Thread.__init__(self)
        self.platform = platform
        self.platformobj = platformobj
        self.delay = delay
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit
        self.radius = randint(30, 70)

    def run(self):
        #o = [-33.84119 , 151.25612]       # fake origin to bounce around
        o = self.platformobj.origin
        i = 0

        while(1) :
            # msgid=time.time()  # timestamp / msgid
            lat, lon, hdg = FakeCoordOnCircle(i, self.radius, o)
            i += 1

            #print platformtrackhistory[self.platform]
            data = {
                'state': 'online',
                'msgts': int(time.time()),
                'pose': {
                    'lat': round(lat, 10),  # round(o[0]+(random()-0.5)/600, 12),  # REQUIRED (decimal degrees)
                    'lon': round(lon, 10),  # round(o[1]+(random()-0.5)/600, 12),  # REQUIRED (decimal degrees)
                    'heading': round(hdg, 1),  # randint(0, 360),                 # REQUIRED (degrees)
                    'uncertainty': float(randint(1, 20)),
                    'speed': 0.5,
                    'depth': round(random()*100, 2),            # OPTIONAL (m)
                    'roll': float(randint(-20, 20)),                   # OPTIONAL / REQUIRED for dashboard (degrees)
                    'pitch': float(randint(-45, 45)),                  # OPTIONAL / REQUIRED for dashboard (degrees)
                    'alt': round(random()*10, 2)                # OPTIONAL (m)
                },
                'stat': {
                    # You can add whatever custom status messages you like here
                    # NB: 'bat' has a special display widget
                    # Any others, just get displayed normally
                    'bat': randint(0, 100),                     # OPTIONAL / REQUIRED for dashboard (int 0-100)
                    'waypt': randint(1, 10),
                    'state': 'OK',
                    'lag': 10
                },
                'alert': {
                    # You can add whatever custom alerts you like here.
                    # Must return 1 for OK, anything else shows as error.
                    'leak': randint(0, 1),
                    'power': randint(0, 1),
                    'state': randint(0, 1),
                    'coms': randint(0, 1),
                    'status': randint(0, 1),
                    'new':1,
                    'lag':0
                }
            }

            # This is adding a custom alert if AUV > 6 m
            if data['pose']['depth'] < 6:
                data['alert']['DEP<6'] = 1

            print data

            self.platformobj.set_data(self.platform, data, )

            # This dynamically updates the mission
            # TODO: SET THIS TO SOMETHING USEFUL
            #self.platformobj.set_platformmission(self.platform, "somefile.mp")

            # Add random data to heatmaps at positions of vehicle
            self.platformobj.add_to_heatmap(self.platform, 'temp', {"lat": data['pose']['lat'], "lng": data['pose']['lon'], "value": random() * 30})
            self.platformobj.add_to_heatmap(self.platform, 'depth', {"lat": data['pose']['lat'], "lng": data['pose']['lon'], "value": random() * 10})

            # Add a random log message. This can be done in any thread.
            msgtype, msg = generate_random_log_message(self.platform)
            self.platformobj.log_msg(msg, msgtype)

            time.sleep(self.delay)


class FakeShipThread (threading.Thread):
    def __init__(self, platform, platformobj, delay):
        threading.Thread.__init__(self)
        self.platform = platform
        self.platformobj = platformobj
        self.delay = delay
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit
        self.radius = randint(30, 70)

    def run(self):
        #o = [-33.84119 , 151.25612]       # fake origin to bounce around
        o = self.platformobj.origin
        i = 0

        while(1) :
            lat, lon, hdg = FakeCoordOnCircle(i, self.radius, o)
            i += 1
            data = {
                'state': 'online',
                'msgts': int(time.time()),
                'pose': {
                    'lat': round(lat, 10),  # round(o[0]+(random()-0.5)/600, 12),  # REQUIRED (decimal degrees)
                    'lon': round(lon, 10),  # round(o[1]+(random()-0.5)/600, 12),  # REQUIRED (decimal degrees)
                    'heading': round(hdg, 1),  # randint(0, 360)                  # REQUIRED (degrees)
                    'uncertainty': 5.0,
                    'speed': 0.5
                },
                'stat': {},
                'alert': {}
            }

            self.platformobj.set_data(self.platform, data, )

            time.sleep(self.delay)

            # Add a random log message. This can be done in any thread.
            msgtype, msg = generate_random_log_message(self.platform)
            self.platformobj.log_msg(msg, msgtype)


def FakeCoordOnCircle(i, radius, o):
    N = 101
    radius += (random()*2-1)
    angle = math.pi*2*(i % N)/N
    dx = radius*math.cos(angle)
    dy = radius*math.sin(angle)
    lat = o[0] + (180/math.pi)*(dy/6378137)
    lon = o[1] + (180/math.pi)*(dx/6378137)/math.cos(o[0]*math.pi/180)
    hdg = (- math.atan2((lat-o[0]), (lon-o[1]))*180/math.pi + 360) % 360

    return lat, lon, hdg


def mission_to_geojson(filepath, platformmission, origin):

    # TODO: do something useful with this
    mission_file = "{}/{}".format(filepath, platformmission)
    print mission_file

    latlngs = [[origin[0]+(random()-0.5)/500, origin[1]+(random()-0.5)/500],
               [origin[0]+(random()-0.5)/500, origin[1]+(random()-0.5)/500],
               [origin[0]+(random()-0.5)/500, origin[1]+(random()-0.5)/500],
               [origin[0]+(random()-0.5)/500, origin[1]+(random()-0.5)/500]]

    mission = FeatureCollection([
        Feature(geometry=Point(origin), properties={
            "options": {"radius": 6, "fillColor": "yellow", "color": "black", "weight": 2, "opacity": 0.5, "fillOpacity": 0.5},  # styling options
            "info": {"Type": "Point: Origin", "Name": platformmission}  # these fields are fully customisable and are displayed in a popover
        }),
        Feature(geometry=Point(latlngs[0]), properties={
            "options": {"radius": 6, "fillColor": "yellow", "color": "white", "weight": 1, "opacity": 1, "fillOpacity": 0.8},  # styling options
            "info": {"Type": "Point: START", "Name": platformmission}  # these fields are fully customisable and are displayed in a popover
        }),
        Feature(geometry=Point(latlngs[-1]), properties={
            "options": {"radius": 6, "fillColor": "yellow", "color": "black", "weight": 1, "opacity": 1, "fillOpacity": 0.8},  # styling options
            "info": {"Type": "Point: END", "Name": platformmission}  # these fields are fully customisable and are displayed in a popover
        }),
        Feature(geometry=LineString(latlngs), properties={
            "options": {"color": "yellow", "weight": 3, "opacity": 0.5, "dashArray": "5,5"},  # styling options
            "info": {"Type": "LineString: Path", "Name": platformmission, "Duration": "5 Hours"}  # these fields are fully customisable and are displayed in a popover
        })
        # Can also include a Polygon in a similar way
    ])

    mission['msgts'] = int(time.time()/30)  # new mission every 30 seconds

    return mission


def generate_random_log_message(noun):
    # content for random messages
    adj = ("dumb", "clueless", "old", "ridiculous", "crazy")
    #nouns = ("KAYAK", "AUV", "ROV", "ROBOT", "DINGY")
    verbs = ("ran", "crashed", "failed", "befuddled", "wigged out")
    adv = ("crazily.", "swimmingly.", "foolishly.", "strangely.", "spectacularly.")
    types = ("success", "warning", "danger", "info")

    message = 'The ' + adj[randrange(0, 5)] + ' ' + noun + ' ' + verbs[randrange(0, 5)] + ' ' + adv[randrange(0, 5)]
    return types[randrange(0, 4)], message


if __name__ == "__main__":
    pf = Platform()
    while True:
        print "--------------------"
        for p in pf.platformdata:
            print "***", p, ":\n",  pf.platformdata[p]
        time.sleep(1)
