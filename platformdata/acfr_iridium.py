######################################################################
# This file handles all the platform pose updates.
#
# There are some TODOS listed throughout the file with instructions.
# See documentation below for more info.
#
# Contact me if you have any questions.
#
# Author:
#   Ariell Friedman
#   ariell@greybits.com.au
#   07 OCT 2016
#
######################################################################

from random import random, randint
import time
import threading
from PlatformBase import PlatformBase


class Platform(PlatformBase):
    origin = [-33.840721, 151.254373]
    interval = 60   # seconds between email checks
    def __init__(self):
        PlatformBase.__init__(self)
        EmailCheckerThread('AUV1.IRIDIUM', self, self.interval).start()


# This thread handles the email checking and the position updating
class EmailCheckerThread (threading.Thread):
    def __init__(self, platform, platformobj, delay):
        threading.Thread.__init__(self)
        self.platform = platform
        self.platformobj = platformobj
        self.delay = delay
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit

    def run(self):
        while 1:
            # TODO: this is where the email checker should set the lat-lon values
            #  - Check email
            #  - Parse attachment
            #  - Get relevant parameters and insert them into structure below

            # NOTE: MAKE SURE THAT EXCEPTIONS ARE HANDLED AND THAT NOTHING IN THIS LOOP WILL BREAK THE THREAD
            # SURROUND IN A TRY-EXCEPT

            # For now, generate a random lat lon
            lat = self.platformobj.origin[0]+(random()-0.5)/500
            lon = self.platformobj.origin[1]+(random()-0.5)/500
            uncertainty = randint(1, 20)   # not sure if you will get this, if not comment it out here and below

            data = {
                'state': 'online',
                'msgts': int(time.time()),  # This should not be changed
                'pose': {
                    'lat': round(lat, 10),                      # REQUIRED (decimal degrees)
                    'lon': round(lon, 10),                      # REQUIRED (decimal degrees)
                    'uncertainty': uncertainty,
                    #'heading': round(hdg, 1),                  # randint(0, 360),
                    #'speed': 0.5,
                    #'depth': round(random()*100, 2),            # OPTIONAL (m)
                    #'roll': randint(-20, 20),                   # OPTIONAL / REQUIRED for dashboard (degrees)
                    #'pitch': randint(-45, 45),                  # OPTIONAL / REQUIRED for dashboard (degrees)
                    #'alt': round(random()*10, 2)                # OPTIONAL (m)
                },
                'stat': {
                    # You can add whatever custom status messages you like here
                    # NB: 'bat' has a special display widget
                    # Any others, just get displayed normally

                    # 'bat': randint(0, 100),                     # OPTIONAL / REQUIRED for dashboard (int 0-100)
                    # 'waypt': randint(1, 10),
                    # 'state': 'OK'
                },
                'alert': {
                    # You can add whatever custom alerts you like here.
                    # Must return 1 for OK, anything else shows as error.

                    # 'leak': randint(0, 1),
                    # 'power': randint(0, 1),
                    # 'state': randint(0, 1),
                    # 'coms': randint(0, 1),
                    # 'status': randint(0, 1)
                }
            }

            # Update platform data
            self.platformobj.set_data(self.platform, data, )
            time.sleep(self.delay)


if __name__ == "__main__":
    pf = Platform()
    while True:
        print "--------------------"
        for p in pf.platformdata:
            print "***", p, ":\n",  pf.platformdata[p]
        time.sleep(1)
