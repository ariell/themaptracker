TMPDIR='tmp'
PLATFORM=`python -c 'from maptracker_utils import check_platform; print check_platform()'`
VERSION=`cat version`
BUILDDIR="build/versions/$VERSION/$PLATFORM"
#SERVERDIR="versions/$VERSION/$PLATFORM"
SERVERDIR="107.180.0.193/greybits.com.au/maptracker/versions/$VERSION/$PLATFORM/"


THISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $THISDIR
pyinstaller --workpath=$TMPDIR --distpath=$BUILDDIR start_webserver.spec

read -p "Would you like to upload the file to the update server? [y/N]: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo -e "\nUploading '$BUILDDIR/start_webserver' to '$SERVERDIR'";
    #curl -v -F "path=$SERVERDIR" -F "file=@$BUILDDIR/start_webserver" http://greybits.com.au/maptracker/upload.php
    read -p "Username: " username;
    read -s -p "Password: " password; echo
    curl --ftp-create-dirs -T $BUILDDIR/start_webserver ftp://$username:$password@$SERVERDIR
fi
echo -e "\nDONE!";

#read -p "Would you like to download build files from the server? [y/N]: " -n 1 -r
#if [[ $REPLY =~ ^[Yy]$ ]]; then
#    rsync -Pavz ubuntu@203.101.232.29:~/maptracker/build/versions/* build/versions
#fi
