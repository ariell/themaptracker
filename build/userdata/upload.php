<?php
// In PHP versions earlier than 4.1.0, $HTTP_POST_FILES should be used instead
// of $_FILES.

$uploaddir = '';
$uploadfile = $uploaddir . basename($_FILES['file']['name']);

echo '{';

if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
    echo '"success":true, "message":"File was successfully uploaded and saved ('.$uploadfile.')."';
} else {
    echo '"success":false, "message":"UNKNOWN ERROR: File could not be saved!"';
}

//echo '"info":"Here is some more debugging info:';
//print_r($_FILES);

print '}';

?>