<?php
// In PHP versions earlier than 4.1.0, $HTTP_POST_FILES should be used instead
// of $_FILES.

//print_r($_POST);
//print_r($_FILES);
//phpinfo();
//exit();

// ensure that dir is in this path by stripping /.
$uploaddir = rtrim(ltrim($_POST["path"], "/."), "/");
if ($uploaddir != "") $uploaddir .= "/";
$uploadfile = $uploaddir . basename($_FILES['file']['name']);


// make dir if it does not exist
if (!is_dir($uploaddir) && $uploaddir) {
    mkdir($uploaddir, 0755, true);
}

if (is_dir($uploaddir) && move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
    echo '{"success":true, "message":"File was successfully uploaded and saved ('.$uploadfile.')."}';
} else {
    echo '{"success":false, "message":"UNKNOWN ERROR: File could not be saved!"}';
}

//echo '"info":"Here is some more debugging info:';
//print_r($_FILES);


?>