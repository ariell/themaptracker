import requests
from time import sleep, time
from demo_fake_data import get_fake_platform_data, get_fake_mission, get_fake_log_message



def post_platformdata(platform_name, data, data_type="platform"):
    maptracker_url = "http://localhost"
    maptracker_port = 8080
    resp = requests.post("{}:{}/data/platformdata.api.Platform/{}?data_type={}".format(
        maptracker_url,maptracker_port,platform_name, data_type), json=data
    )
    # Debug print
    print ("Type: {data_type}, Response: {resp[response]} ({code}), Message: {resp[msg]}".format(
        resp=resp.json(), code=resp.status_code, data_type=data_type)
    )
    return resp


if __name__ == "__main__":
    # Name of platform. You can post multiple platforms and missions to the same endpoint, but the name must be unique.
    platform_key = "test"

    # Fake origin used to generate fake pose and fake mission
    fake_origin = [-33.8405831822, 151.2548891302]

    mission_upd_time = None
    while True:
        # Generate and send PLATFORM DATA
        # This would happen whenever you receive an update from the vehicle, and should probably be in a thread
        platform_data = get_fake_platform_data(fake_origin)

        resp = post_platformdata(platform_key, platform_data, data_type="platform")

        # if error response, log to logger console (needs to be added to map to view)
        # if resp.status_code != 200:
        #     msg = "Error: {resp[msg]}".format(resp=resp.json())
        #     post_platformdata(platform_key, {"msg": msg, "type": "danger"}, data_type="log")


        # Example of generating a log message
        msg_type, msg = get_fake_log_message()
        msg_data = {"msg": msg, "type": msg_type}
        post_platformdata(platform_key, msg_data, data_type="log")

        # Generate and send MISSION DATA
        # For this demo, we posting a new mission to every 30 seconds as a proof of concept.
        # In practice, this should be done every time the active mission file is updated.
        if mission_upd_time is None or time() - mission_upd_time > 30:
            mission_data = get_fake_mission(fake_origin)
            post_platformdata(platform_key, mission_data, data_type="mission")
            mission_upd_time = time()

        # repeat every 2s
        sleep(2)
