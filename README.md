# The Map Tracker #

Web-based multi-vehicle mission status and tracking system

* Multi-platform tracking
* Local onboard instance
* Multi-layer map support
* Access to online hosted instance
* Multi-platform logging
* Social media widgets
* Video streaming widgets
* iFrame website integration

## Online webserver deployment (non-standalone) ##
```
#!bash
sudo apt-get install python-dev libjpeg8-dev
sudo apt-get install python-gdal libgdal-dev  # required for offline maps
sudo apt-get build-dep python-gdal   # NOTE: need source code in ubuntu software sources checked
git clone https://ariell@bitbucket.org/ariell/themaptracker.git
sudo pip install -r requirements.txt
```

Note: if you get errors with gdal, try: 
```pip install GDAL==$(gdal-config --version | awk -F'[.]' '{print $1"."$2}')  --global-option=build_ext --global-option="-I/usr/include/gdal/"``` 
OR
```pip install GDAL==$(gdal-config --version | awk -F'[.]' '{print $1"."$2}')```

### Running the server ###

```
#!bash

python start_webserver.py
```

### Connecting to the GUI ###
If on the same machine, connect to:

**http://localhost:8080**

By default it broadcasts across the network, so you can connect to it from other devices:

**http://<ip_address>:8080**


## Standalone packaged deployment ##

### Download maptracker: manual download option ###

To get the latest links, from the cli type:

```
#!bash

curl -s http://greybits.com.au/maptracker/current_version.json
```

This should return something like:

```
{
    "version": 1.0,
    "platform": {
        "linux":"http://203.101.232.29/static/maptracker/build/versions/1.0/linux/start_webserver",
        "osx":"http://203.101.232.29/static/maptracker/build/versions/1.0/osx/start_webserver",
        "windows":"http://203.101.232.29/static/maptracker/build/versions/1.0/windows/start_webserver"
    }
}
```
Then, using the appropriate link for your OS above, run:

```
#!bash

mkdir maptracker && cd maptracker   # do this where you want to install maptracker
wget "http://203.101.232.29/static/maptracker/build/versions/1.0/linux/start_webserver"  # for linux
#wget "http://203.101.232.29/static/maptracker/build/versions/1.0/osx/start_webserver"  # for osx
chmod +x start_webserver
```

### Download maptracker: auto-download script option ###

```
#!bash

mkdir maptracker && cd maptracker   # do this where you want to install maptracker
curl http://greybits.com.au/maptracker/download | python
```


### Running the local maptracker server ###

The maptracker is contained in a single, stand-alone executable. To run the maptracker, type:

```
#!bash

./start_webserver
```

If on the same machine that is running the server, view it by opening a web-browser to navigating:

**http://localhost:8080**

By default it broadcasts across the network, so you can connect to it from other devices:

**http://<ip_address>:8080**

where **<ip_address>** is the IP of the server machine.

Once set up, it will initialise the directories and files required to run the maptracker, create a server config file and start the server. You will notice some new directories in the maptracker directory:

```
config/
datatools/
platformdata/
templates/
uploads/
```

You can then install your own modules, plugins and configs by copying them into the appropriate directories and configure the layers by changing the config files in the GUI.

[comment]: <> (### License key ###)
[comment]: <> ()
[comment]: <> (For the first run only, you will be asked for a licence key. The key should have been provided via email and is case sensitive. This key is used to activate the maptracker and it is also used to send data to the remote server for your online account. Do not share that key with anyone. They licence key is also used to communicate with the online remote server for logging and cruise history.)
[comment]: <> ()
[comment]: <> (### Downloading custom modules / configs ###)
[comment]: <> ()
[comment]: <> (You should have a secure directory set up on the server to download your custom modules for your platforms. To browse your files, navigate to the link provided, or alternatively download them using:)
[comment]: <> ()
[comment]: <> (```)
[comment]: <> (#!bash)
[comment]: <> ($USRDIR='')
[comment]: <> ($USR='')
[comment]: <> ($PASS='')
[comment]: <> (wget -r -np -nH --cut-dirs=4 -R index.html* http://203.101.232.29/static/maptracker/build/userdata/$USRDIR/ --user='$USR' --password='$PASS')
[comment]: <> (```)

Where ```$USRDIR``` should be set to your allocated user directory, ```$USR``` is your supplied username and ```$PASS``` is your supplied password.
Once downloaded, copy each of the files to the matching directories created above in the maptracker directory, and then load the config files to activate the different modules and layers.

### Updating maptracker

*WARNING: As a precaution, you may wish to make a backup of your existing maptracker directory before following the steps below.*

1. Start server  ```./start_webserver```
1. If an update is available, you will be prompted. Type ```YES``` to update
1. Once complete, start server again  ```./start_webserver```
1. In the gui, click ```Settings > My Data > Upload``` to backup your current user files
1. Then click ```Settings > My Data > Reset``` to reset prepackaged files with defaults
1. Restart the server

### Running API demo
Maptracker now comes bundled with a new api plugin: ```platformdata/api.py```
This plugin makes it possible to integrate your external scripts and applications into maptracker and supports multiple platforms. 
It currently accepts posting of platform data, dynamic missions and log messages (for display in the GUI).
It validates inputs and provides detailed messages in response to errors.

1. To run API test / demo, you need to add the test layers. Click ```Settings > Layers / Config```, and add the following layers:
    * ```targets > api-test.json```
    * ```missions > api-test_mission.json```
    * ```widgets > logconsole-api.json```
    * You may wish to deactivate any layers not being used (to unclutter interface)
1. In a separate terminal window, run the external demo script to post data to the API:
```python external_scripts/demo_api_post.py```
    * This script depends upon the ```demo_fake_data.py``` file in the same directory and also requires the ```geojson``` python module, which can be obtained using ```pip```.
    * You should now see messages posting in the terminal window.
1. If all went to plan, you should also now see the layers updating in the maptracker GUI.

For more info on how to implement your own API-integrated platforms, check out the example code in the
```demo_api_post.py``` file. Comments in the code should be self explanatory, but there will be more formal documentation coming soon.

[comment]: <> (# Admin stuff #)
[comment]: <> ()
[comment]: <> (## Rebuild ##)
[comment]: <> ()
[comment]: <> (1. Ensure that everything is properly set up in)
[comment]: <> (```start_webserver.spec```.)
[comment]: <> (2. To compile on OSX, run ```./build_executable.sh``` and allow auto upload to webserver at the end.)
[comment]: <> (3. To compile for linux, log in to remote server: ```ssh ubuntu@203.101.232.29; cd maptracker; ./build_executable.sh```. Do NOT allow it to upload at the end.)
[comment]: <> (4. Windows machine????)
[comment]: <> (5. Update ```build/versions/current_version.json```, then log in to [hosting account]\(https://a2nwvpweb108.shr.prod.iad2.secureserver.net:8443/smb/file-manager/list?currentDir=%2F%2Fgreybits.com.au%2Fmaptracker\) and replace current version file at: http://greybits.com.au/maptracker/current_version.json)
[comment]: <> ()
[comment]: <> (## Add new private client directory ##)
[comment]: <> ()
[comment]: <> (```)
[comment]: <> (#!bash)
[comment]: <> (ssh ubuntu@203.101.232.29)
[comment]: <> (cd maptracker/build/userdata)
[comment]: <> (./.passwords/create_new_client_dir.sh   # this creates a new user and a new user directory)
[comment]: <> (```)