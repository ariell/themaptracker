import sys
import datetime
from maptracker_utils import license_params
import base64


def generate_license(email, days, startdate=None):
    if startdate is None:
        startdate = datetime.datetime.now()
    datestr = (startdate + datetime.timedelta(days=int(days))).strftime(license_params['date_format'])
    license_string = (email+"|"+datestr).rjust(48)
    license_key = base64.b64encode(license_params['cipher'].encrypt(license_string))
    print "Generating licence for: {}\nValid for: {} days\nFrom: {} \nUntil: {}\nKEY: {}"\
        .format(email, days, startdate.strftime(license_params['date_format']), datestr, license_key)
    return license_key


if __name__ == '__main__':
    try:
        email = sys.argv[1]
        days = sys.argv[2]
        generate_license(email, days)
    except Exception:
        print "Generate license key for map tracker.\nUsage: \n    python generate_license.py <email> <num_days>\n"