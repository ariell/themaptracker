# This file contains the WSGI configuration required to serve up your
# web application. It works by setting the variable 'application' to a
# WSGI handler of some description.

import sys
import os

# add your project directory to the sys.path
project_home = os.path.dirname(os.path.realpath(__file__))

# activate virtual environment
activate_this = project_home+'/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

if project_home not in sys.path:
    sys.path = [project_home] + sys.path

# import flask app but need to call it "application" for WSGI to work
from start_webserver import app as application