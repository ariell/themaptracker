/**********************************************
 *
 * AUV Platform mapper
 *
 * Author:
 *      Ariell Friedman
 *      ariell.friedman@gmail.com
 *      25 AUG 2014
 *
 *********************************************/

/**
 * Constructor
 */

function auvmapper () {
    var _this = this;   // for convenient use in different contexts
    // TODO: do not hard code
    //this.origin = [-33.889643, 151.193483]; // default origin ACFR
    //this.origin = [41.492421, -71.420121];  // default origin URI
    this.origin = [20.73408089, -156.66]; // default origin AU-AU channel, hawaii
    this.maxposes = 100; // maximum number of poses in a platform track (to avoid memory/performance issues)

    // Initialise layers and info
    this.layers = {};
    this.layers.base = {};
    this.layers.overlays = {};
    this.info = {};
    this.dash = {};
    this.snap_to_track = {};
    this.autotrack_layer = {};
    this.mission_ts = {};
    this.targetlist = []; // list of targets

    this.baselayeradded = false;

    /**
     * Intialise the map and layers
     * @param mapid
     */
    this.init = function(mapid) {
        this.mapid = mapid;
        this.fullsize();
        this.map = new L.Map(mapid, {
            center: new L.LatLng(this.origin[0],this.origin[1]),
            zoom: 18,
            maxZoom: 25,  // max zoom is more than native zoom - leads to pixelated tiles
            maxNativeZoom: 17 //,
            // fullscreenControl: true
        });

        // add blank tile layer
        this.layers.base = {'None': L.tileLayer('')};


        // Add layer control
        this.layerctl = new L.control.layers(this.layers.base, this.layers.overlays, {autoZIndex: true}).addTo(this.map);
        L.control.scale({imperial:false, metric:true}).addTo(this.map);


        // Deactivate auto-extent on map drag
        this.map.on("dragstart",function(){
            _this.auto_extent ("all", false);
        });

        // Add permalink control
        //this.map.addControl(new L.Control.Permalink({useLocation: true}));

        // Disable scrollwheel zoom
        // this.map.scrollWheelZoom.disable();


        // Add length measurement control
        L.Control.measureControl().addTo(this.map);
        $(".leaflet-control-fullscreen-button").tooltip({placement:"right",trigger:"hover",container:"body"});


        // Add range-bearing measure tool
        // L.control.polylineMeasure({position:'topleft', unit:'metres', showBearings:true, clearMeasurementsOnStop: true, showMeasurementsClearControl: false, showUnitControl: true}).addTo(this.map);


        // Add mouse position
        L.control.mousePosition({numDigits: 8}).addTo(this.map);
        $(".leaflet-control-draw-measure").tooltip({placement:"right",trigger:"hover",container:"body"});


        // // prevent scrolling of map when on control
        // $(".leaflet-bottom.leaflet-right").mouseover(function(){
        //     //this.map.dragging.disable();
        //     _this.map.touchZoom.disable();
        //     _this.map.doubleClickZoom.disable();
        //     _this.map.scrollWheelZoom.disable();
        // }).mouseout(function(){
        //     //this.map.dragging.disable();
        //     _this.map.touchZoom.enable();
        //     _this.map.doubleClickZoom.enable();
        //     _this.map.scrollWheelZoom.enable();
        // });

        /*
        // TEST: control to get latlng - WIP
        this.add_control("o",function(){
            _this.map.on('click', function(e) {
                alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
            });
        },"Demo click latlng");
        */

        // Initialise layer group for auto tracking
        this.autotrack_layer = new L.featureGroup([]).addTo(this.map);
    };

    /**
     * Get load the mission layer from an ajax call.
     * If unsuccessful, it retries every 5 seconds.
     * @param layer
     */
    this.get_mission = function(layer, filepath, url, usroptions, origin, create_new){
        if (typeof usroptions == "undefined") usroptions = {};
        if (typeof origin == "undefined") origin = [0,0];
        if (typeof create_new == "undefined") create_new = true;
        var options = { color: 'red', weight: 3, opacity: 0.5, smoothFactor: 1, interval: 30000, clearold: true};
        $.extend(options,usroptions);

        if (!_this.mission_ts.hasOwnProperty(layer)) _this.mission_ts[layer] = 0;

        $.ajax({
            dataType: "jsonp",
            url: url,
            data: {filepath: filepath, olat: origin[0], olon: origin[1]},
            success: function (data) {
                //console.log(data);
                if (data == null) {
                    failed(layer, filepath, url, usroptions, origin, "No valid mission data...");
                    return;
                }
                else if (parseInt(_this.mission_ts[layer]) < parseInt(data.msgts)) {
                    _this.mission_ts[layer] = data.msgts;
                    if (Object.keys(data).length !== 0) {

                        if (options.clearold && _this.layers.overlays.hasOwnProperty(layer))
                            _this.layers.overlays[layer].clearLayers();
                        // set mission
                        //_this.layers.overlays[layer] = new L.Polyline([], options).addTo(_this.map);
                        _this.layers.overlays[layer] = new L.geoJson(data, {
                            style: function (feature) {
                                //console.log(feature);
                                return feature.properties.options;
                            },
                            onEachFeature: function (feature, layer) {
                                layer.bindPopup(formatdata(feature.properties.info));
                            },
                            pointToLayer: function (feature, latlng) {
                                return L.circleMarker(latlng, feature.properties.options);
                            }

                        }).addTo(_this.map);

                        if (create_new) {
                            _this.layerctl.addOverlay(_this.layers.overlays[layer], layer);

                            //_this.layers.overlays[layer].bindPopup(layer);

                            $("#track-layers").prepend($("<li><a href='#' style='white-space: nowrap;'><i class='fa fa-slack' style='display: inline-block'></i> Mission: " + layer + "</a></li>").click(function () {
                                _this.auto_extent("all", false);
                                _this.map.fitBounds(_this.layers.overlays[layer].getBounds());
                            }));
                        }
                        // set mission
                        //_this.layers.overlays[layer].setLatLngs(data.latlngs);
                        //_this.map.fitBounds(_this.layers.overlays[layer].getBounds());

                        // set origin
                        _this.origin = data.origin;
                    }
                }

                // reload mission
                if (options.interval > 0)
                    setTimeout(function(){_this.get_mission(layer, filepath, url, usroptions, origin, false)},options.interval);

            },
            error : function (jqXHR, status, desc) {
                failed(layer, filepath, url, usroptions, origin, jqXHR);
            }
        });

        function failed(layer, filepath, url, usroptions, origin, msg){
            console.log("Cannot load mission: "+filepath,msg);
            setTimeout(function(){_this.get_mission(layer, filepath, url, usroptions, origin)},5000); // try again in 5 seconds
        }
    };


    /**
     * Load a geotiff from a url
     * @param layer: name of layer (no spaces)
     * @param url: url of geotiff
     */
    this.get_geotiff = function(layer, imgurl, url){
        $.ajax({
            dataType: "jsonp",
            url: url,
            data: {url: imgurl},
            success: function (data) {
                $("#track-layers").prepend($("<li><i class='fa fa-picture-o'> Img: "+layer+"</i></li>").click(function(){
                    _this.auto_extent ("all", false);
                    _this.map.fitBounds(L.latLngBounds(data.bounds));
                }));
                _this.layers.overlays[layer] = new L.imageOverlay(data.imgurl, data.bounds);//.addTo(_this.map).bringToBack();
                _this.map.addLayer(_this.layers.overlays[layer], true);
                _this.layerctl.addOverlay(_this.layers.overlays[layer].bringToBack(), layer);
            },
            error : function (jqXHR, status, desc) {
                console.log("Cannot load geotiff: "+imgurl,jqXHR);
                setTimeout(function(){_this.get_geotiff(layer, imgurl, url)},5000); // try again in 5 seconds
            }
        });
    };

    this.get_kml = function(layer, filepath){

        _this.layers.overlays[layer] = new L.KML(filepath, {async: true});
        _this.layers.overlays[layer].addTo(this.map).on("loaded", function(e) {
            $("#track-layers").prepend($("<li><i class='fa fa-file-image-o'> KML: "+layer+"</i></li>").click(function(){
                _this.auto_extent ("all", false);
                _this.map.fitBounds(e.target.getBounds());
            }));
            _this.layerctl.addOverlay(_this.layers.overlays[layer].bringToBack(), layer);
        });
    };


    this.get_heatmap = function(data) {
        console.log(data);
        _this.layers.overlays[data.settings.name] = new HeatmapOverlay(data.settings.options);
        _this.layerctl.addOverlay(_this.layers.overlays[data.settings.name], data.settings.name);
        if (data.settings.addtomap) _this.layers.overlays[data.settings.name].addTo(_this.map);
        update_heatmap(_this.layers.overlays[data.settings.name], data.settings.url, data.settings.interval);

        function update_heatmap(lyr, url, interval){
            $.getJSON(url, function(jsondata){
                console.log(jsondata);
                lyr.setData(jsondata);
                setTimeout(function(){
                    update_heatmap(lyr, url, interval);
                }, interval);
            });
        }
    };



    /**
     * Create pose tracking layer
     * @param platform
     * @param url
     * @param interval
     * @param trackoptions
     * @param markeroptions
     * @param showdash
     */
    this.add_posetracker = function(platform, url, usroptions, usrdispoptions) {
        if (typeof usroptions == "undefined") usroptions = {};
        if (typeof usrdispoptions == "undefined") usrdispoptions = {};
        var dispoptions = {showdash: false, showrph: false, showbatt: false, showtrack: false, setstate: false}; // default dispoptions
        var options = {color: 'red', interval: 1000, size: 3, maxtracklen: 100};  // default other otpions
        $.extend(dispoptions,usrdispoptions);  // extend to make sure all fields
        //usroptions.dispoptions = dispoptions;  // copy to usroptions
        $.extend(options,usroptions); // extend options

        var trackoptions={color: options.color, weight: 1, opacity: 0.9, smoothFactor: 1 },
            markeroptions = {color: options.color, weight: 2.5, fillColor: "black", fillOpacity: 0.5, opacity: 1, zIndexOffset: 1000},
            polyoptions = {color: options.color,weight:2.5,fillColor:"white",fillOpacity:0.5,opacity:1},
            uncmarkeroptions = {color: options.color, weight: 0.4, fillColor: options.color, fillOpacity: 0.2, opacity: 0.4, zIndexOffset: 1000, className: "nomouse"};

        var tracklayer = platform.key+" track",
            unclayer = platform.key+" uncertainty";

        // add track layer
        if (dispoptions.showtrack) {
            this.layers.overlays[tracklayer] = new L.Polyline([], trackoptions).addTo(this.map);
            this.layerctl.addOverlay(this.layers.overlays[tracklayer], platform.name+" track");
            //console.log(this.layers.overlays[tracklayer]);
            // Add fancy mid line markers
            //var markerid = 'circle-'+platform.key.replace(' ','_');
            //$(this.layers.overlays[tracklayer]._container).prepend('<marker id="'+markerid+'" markerWidth="6" markerHeight="6" refX="3" refY="3" markerUnits="userSpaceOnUse"><circle cx="3" cy="3" r="3" fill="'+options.color+'"></circle></marker>');
            //$(this.layers.overlays[tracklayer]._path).attr("marker-mid","url(#"+markerid+")");
        }

        // if size is an object, then draw a ship, otherwise draw a circle
        if (typeof options.size === "number") {
            this.layers.overlays[platform.key] = new L.circle(this.origin, options.size, markeroptions);
        }
        else if (typeof options.size === "object") {
            this.layers.overlays[platform.key] = new L.polygon([],polyoptions);
            this.layers.overlays[platform.key].poly = new getShipPoly(options.size, this.layers.overlays[platform.key]);
            this.layers.overlays[platform.key].poly.setLatLngHdg(0,new L.LatLng(this.origin[0],this.origin[1]));
        }
        // uncertainty layer
        this.layers.overlays[unclayer] = new L.circle(this.origin, 1, uncmarkeroptions);

        // Add popup
        this.layers.overlays[platform.key].bindPopup(platform.name);

        // add to layer group (platform.key & unc) to control
        var layergroup = new L.layerGroup([this.layers.overlays[unclayer],this.layers.overlays[platform.key]]);
        layergroup.addTo(this.map);
        this.layerctl.addOverlay(layergroup, platform.name);

        // initialise info panel
        this.snap_to_track[platform.key] = false;
        this.add_platform_controls(platform, tracklayer, markeroptions.color, dispoptions);
        this.add_platform_dashboard(platform, markeroptions.color, dispoptions);

        // start position updater
        // get track history for first update
        url += (url.indexOf('?') == -1 ? '?' : '&') + "gethistory=true";
        this.update_posetracker(tracklayer, unclayer, platform, url, options.interval, options.maxtracklen);
    };

    /**
     * Updates pose tracking layer at regular interval.
     * If fails, retries every 5 seconds.
     * @param tracklayer
     * @param platform
     * @param url
     * @param interval
     * @param unclayer
     * @param maxtracklen
     */
    this.update_posetracker = function( tracklayer, unclayer,  platform, url, interval, maxtracklen) {
        //var _this = this;
        if ($(_this.info[platform.key]).parent().find(".errmsg").length <= 0) $(_this.info[platform.key]).parent().append("<div class='error errmsg' data-count='0'></div>");
        var $status_message = $(_this.info[platform.key]).parent().find(".errmsg");

        $.ajax({
            dataType: "jsonp",
            url: url,
            timeout: 20000, // sets timeout to 20 seconds
            success: function (data) {


                //console.log(data.statemsg);
                if (parseInt(_this.info[platform.key].data('msgts')) < parseInt(data.msgts)) {
                    _this.info[platform.key].data('msgts',data.msgts);

                    data.pose = set_pose(platform, tracklayer, unclayer, maxtracklen, data);

                    // If we are tracking, check bounds and move map to track items
                    if (_this.autotrack_layer.getLayers().length > 0) {
                        if (!_this.map.getBounds().contains(_this.autotrack_layer.getBounds())) {
                            // if tracking more than 1 layer, set bounds, otherwise simply pan map
                            //_this.map.fitBounds(_this.autotrack_layer.getBounds());
                            if (_this.autotrack_layer.getLayers().length > 1) _this.map.fitBounds(_this.autotrack_layer.getBounds());
                            else if (_this.autotrack_layer.getLayers()[0].hasOwnProperty("poly")) _this.map.panTo(_this.autotrack_layer.getLayers()[0].getLatLngs()[0]);
                            else _this.map.panTo(_this.autotrack_layer.getLayers()[0].getLatLng());
                        }
                    }


                    // if autohidden then reshow when it comes back online
                    if ($(_this.dash[platform.key]).is(":visible") && $(window).width() < 600) {
                        $(_this.dash[platform.key]).data('autohide', true);
                        $(_this.dash[platform.key]).hide();
                    }
                    else if (data.state == 'online' && $(_this.dash[platform.key]).data('autohide') && $(window).width() >= 600) {
                        $(_this.dash[platform.key]).show();
                        $(_this.dash[platform.key]).data('autohide', false);
                    }
                    // update dashboard if it is visible (and exists)
                    if ($(_this.dash[platform.key]).is(":visible")) {
                        if (data.state != 'online')  {
                            $(_this.dash[platform.key]).data('autohide', true);
                            $(_this.dash[platform.key]).hide();
                        }
                        else {
                            var pose = data.pose,
                                stat = data.stat,
                                alert = data.alert;
                            if ($(_this.dash[platform.key]).find(".hdg-rol").length > 0 && data.hasOwnProperty('pose')) {  //update roll-pitch-heading widget (if visible)
                                $(_this.dash[platform.key]).find(".hdg").val(pose.heading).trigger('change'); // update heading vis
                                $(_this.dash[platform.key]).find(".rol").val(pose.roll).trigger('change'); // update roll vis
                                $(_this.dash[platform.key]).find(".rol-canvas").css("top", 17 - pose.pitch * 33 / 90); // update pitch vis
                                //$(_this.dash[platform.key]).find(".rol-canvas").css("top", 17 - 180 * 33 / 90); // update pitch vis
                                $(_this.dash[platform.key]).find(".rph-info").html(formatdata({HDG: pose.heading, PITCH: pose.pitch, ROLL: pose.roll}));
                                delete pose.roll;
                                delete pose.heading;
                                delete pose.pitch;  // remove fields to avoid duplicate displays
                            }
                            if ($(_this.dash[platform.key]).find(".bat").length > 0 && data.hasOwnProperty('stat')) {  // update battery widget (if visible)
                                if (stat.hasOwnProperty('bat')) {
                                    $(_this.dash[platform.key]).find(".bat").css("width", stat.bat + "%").html(stat.bat + "%");
                                    delete stat.bat;  // remove fields to avoid duplicate displays
                                }
                            }

                            // Show remaining data on dashboard
                            $(_this.dash[platform.key]).find(".platform-alerts").html(formatdata(alert, "alerts"));
                            $(_this.dash[platform.key]).find(".platform-stat").html(formatdata(stat));
                            $(_this.dash[platform.key]).find(".platform-pose").html(formatdata(pose));
                            $(_this.info[platform.key]).html(""); // clear info panel
                        }
                    }
                    else {
                        // make info object by joining pose and stat (if stat exists)
                        if (data.state == 'online') {
                            var info = (data.hasOwnProperty("stat")) ? $.extend(data.pose, data.stat) : data.pose;
                            $(_this.info[platform.key]).html(formatdata(info));
                        }
                        else {
                            $(_this.info[platform.key]).html(""); // clear info panel
                        }
                    }
                    if (data.hasOwnProperty('statemsg')) {
                        $status_message.html("<b style='color:#333'>" + data.statemsg + "</b>");
                    }
                    else {
                        $status_message.html("").data('count',0);  // clear status message
                    }
                }
                else { // if old msg id, show message age
                    var msgage = Math.round(data.curts - data.msgts);
                    $status_message.html("<b style='color:rgb(197, 135, 0)'><small>LASTUPD:</small><br>"+msgage+" s</b>");
                    //if (msgage > 3*60) $(_this.info[platform.key]).parent().css('background-color','#FFCCCC');
                    //else if (msgage > 30) $(_this.info[platform.key]).parent().css('background-color','#CCC');
                    if (msgage > 30) $(_this.info[platform.key]).parent().css('background-color','#CCC');
                    set_uncertainty(unclayer, data.pose);
                    //setTimeout(function(){$(_this.info[platform.key]).parent().css('background-color','white')}, 250);
                }
                var $flashupd = $(_this.info[platform.key]).parent().find('.heartbeat').show();
                setTimeout(function(){$flashupd.hide();},250);
                if (url.indexOf("gethistory") > 0) url=url.substring(0, url.indexOf("gethistory")-1);
                //setTimeout(function(){_this.update_posetracker(tracklayer, unclayer, platform, url, interval, maxtracklen)},interval);
            },
            error : function (jqXHR, status, desc) {
                console.log("Cannot update position: "+platform.key,jqXHR);
                $status_message.data('count',$status_message.data('count')+1)
                    .html("Offline ("+$status_message.data('count')+")");
                $(_this.info[platform.key]).parent().css('background-color','#CCC');
                //setTimeout(function(){_this.update_posetracker(tracklayer, unclayer, platform, url, interval, maxtracklen)},20000); // try again in 20 seconds if error
            }
        }).always(function(){
            setTimeout(function(){_this.update_posetracker(tracklayer, unclayer, platform, url, interval, maxtracklen)},interval);
        });
    };

    function set_pose(platform, tracklayer, unclayer, maxtracklen, data) {
        var pose = (data.state == "follow") ? _this.info[data.follow].data('pose') : data.pose;
        //_this.info[platform.key].data('msgts');
        try {

            //var pose = data.pose;
            var curpos = [], path = [];
            if (pose.lat !== null && pose.lon !== null) {
                if (pose.lat.length > 1 && pose.lon.length > 1) {
                    for (var i = pose.lat.length - 1; i >= 0; i--) path.push(new L.LatLng(pose.lat[i], pose.lon[i]));
                    pose.lat = pose.lat[0];
                    pose.lon = pose.lon[0];
                }
                else if (data.hasOwnProperty("pose_history")) {
                    if (data.pose_history != null)
                        for (var i = data.pose_history.length - 1; i >= 0; i--)
                            path.push(new L.LatLng(data.pose_history[i][0], data.pose_history[i][1]));
                    else
                        path = new L.LatLng(pose.lat, pose.lon);
                }
                else {
                    path = new L.LatLng(pose.lat, pose.lon);
                }
                curpos = new L.LatLng(pose.lat, pose.lon);

                // Add pose to track, but check if track is too long (to avoid memory/performance issues)
                if (data.state == "online") {
                    $(_this.info[platform.key]).parent().css('background-color', '#FFF');
                    if (_this.layers.overlays.hasOwnProperty(tracklayer)) {
                        if (Array.isArray(path)) {
                            _this.layers.overlays[tracklayer].setLatLngs(path);
                        }
                        else {
                            _this.layers.overlays[tracklayer].addLatLng(curpos);
                            var tracklen = _this.layers.overlays[tracklayer].getLatLngs().length;
                            if (tracklen > maxtracklen)
                                _this.layers.overlays[tracklayer].setLatLngs(_this.layers.overlays[tracklayer].getLatLngs().slice(tracklen - maxtracklen, tracklen));
                        }
                    }
                }
                else {
                    $(_this.info[platform.key]).parent().css('background-color', '#CCC');
                    _this.layers.overlays[tracklayer].setLatLngs([]);
                }


                // set uncertainty circle
                set_uncertainty(unclayer, pose);
                //            var uncertainty = (pose.hasOwnProperty('uncertainty')) ? pose.uncertainty : 0.1;
                //            _this.layers.overlays[unclayer].setLatLng(curpos).setRadius(uncertainty);

                // Update marker / polygon position
                if (_this.layers.overlays[platform.key].hasOwnProperty("poly")) {
                    var hdg = (pose.hasOwnProperty("heading")) ? pose.heading : 0;  // use heading if/when available, otherwise default to 0
                    _this.layers.overlays[platform.key].poly.setLatLngHdg(hdg, curpos);//.bringToFront();
                }
                else
                    _this.layers.overlays[platform.key].setLatLng(curpos);

                _this.info[platform.key].data('pose', pose);
            }
        }
        catch(err) {
            console.log(err);
        }
        return pose;
    }


    function set_uncertainty(unclayer, pose) {
        if (typeof pose !== "undefined") {
            if (pose.lat != null && pose.lon != null) {
                var curpos = new L.LatLng(pose.lat, pose.lon);

                // set uncertainty circle
                var uncertainty = (pose.hasOwnProperty('uncertainty')) ? pose.uncertainty : 0.1;
                _this.layers.overlays[unclayer].setLatLng(curpos).setRadius(uncertainty);
            }
        }
    }

    /**
     * Formats data for display.
     * @param data
     * @param format
     * @returns {string}
     */
    function formatdata (data, format) {
        if (typeof format === "undefined") format = "info";
        var datastr = "";
        var dataval = 0;
        if (format == "info") {
            for (var key in data) {
                dataval = data[key];
                //if (! isNaN(dataval)) dataval = dataval.toFixed(4);
                datastr += "<b>" + key + "</b>: " + dataval + "<br>";
            }
        }
        else if (format == "alerts") {
            for (var key in data) {
                if (data[key] == 1)
                    datastr += "<div class='alert alert-danger'>" + key + "</div>";
                    //datastr += "<div class='alert alert-success'>" + key + ": <b>OK</b></div>";
                else
                    datastr += "<div class='alert alert-success'>" + key + "</div>";
                    //datastr += "<div class='alert alert-danger'>" + key;// + ":<br><small>" + data[key] + "</small></div>";
            }
        }
        return datastr;
    }


    /**
     * Set map to full size
     */
    this.fullsize = function () {
        $("#"+this.mapid).css({width:$(window).width(),height:$(window).height()});
    };


    /**
     * Add controls for platform layer
     * @param platform
     * @param tracklayer
     * @param bgcol
     */
    this.add_platform_controls = function(platform, tracklayer, bgcol, dispoptions) {
        var ctl = L.Control.extend({
            options: {
                position: 'bottomleft'
            },
            onAdd: function (map) {
                var ctldiv = L.DomUtil.create('div', 'platform-panel');
                var $ctrlbtns = $("<a href='#' class='pbtns'><i class='fa fa-chevron-circle-left pbtn-hover platform-ctrl'></i></a>");
                $(ctldiv).prepend(
                    $("<div class='pbtns-cont'></div>").append($ctrlbtns),
                    $("<b class='pname' style='cursor: pointer; cursor: hand;'><i class='fa fa-dot-circle-o platform-icon' style='color: "+bgcol+"'></i> "+platform.name+" <i class='fa fa-circle heartbeat'></i></b>")
                        .click(function(el){
                            // if ($(_this.info[platform.key]).is(":visible")) {
                            //     $(this).css("color","#999");
                            //     $(_this.info[platform.key]).hide();
                            // }
                            // else {
                            //     $(this).css("color", "inherit");
                            //     $(_this.info[platform.key]).show();
                            // }
                            $(_this.info[platform.key]).toggleClass("expanded");
                        })
                        .tooltip({title:"Pin/unpin info",trigger:"hover",container:"body"})
                );
                $ctrlbtns.append($("<i class='fa fa-crosshairs platform-ctrl' id='snap-"+platform.key.replace(" ","_")+"'></i>")
                        .click(function(){_this.auto_extent(platform.key)})
                        .tooltip({title:"Keep in view",trigger:"hover",container:"body"}));
                if (dispoptions.showtrack)
                    $ctrlbtns.append($("<i class='fa fa-tencent-weibo platform-ctrl'></i>")
                        .click(function(){_this.layers.overlays[tracklayer].setLatLngs([])})
                        .tooltip({title:"Clear track history",trigger:"hover",container:"body"})
                    );
                if (platform.hasOwnProperty('commandurl') && platform.commandurl !== '')
                    $ctrlbtns.append($("<i class='fa fa-exclamation-circle platform-ctrl'></i>")
                        .click(function() {
                            // TODO: fix this absolute referencing of modal id
                            $("#modalcontent").load(platform.commandurl);
                            $("#modal").modal("show");
                        }).tooltip({title:"Send COMMAND!",trigger:"hover",container:"body"})
                    );
                if (dispoptions.setstate)
                    $ctrlbtns.append($("<i class='fa fa-external-link-square platform-ctrl'></i>")
                        .click(function() {
                            // TODO: fix this absolute referencing of modal id
                            $("#modalcontent").load(platform.url+"?getform=true");
                            $("#modal").modal("show");
                        }).tooltip({title:"Set STATE!",trigger:"hover",container:"body"})
                    );
                _this.info[platform.key] = $("<div class='info'></div>"); // empty div to update with platform info
                _this.info[platform.key].data('msgts',0); // initialise msgid
                $(ctldiv).append(_this.info[platform.key]);
                //$(ctldiv).css("background-color",bgcol);

                return ctldiv;
            }
        });
        this.map.addControl(new ctl());
    };


    /**
     * Add platform dashboard
     * @param platform
     */
    this.add_platform_dashboard = function(platform, bgcol, dispoptions) {
        if (dispoptions.showdash) {
            var ctl;
            ctl = L.Control.extend({
                options: {
                    position: 'bottomright'
                },
                onAdd: function (map) {
                    var ctldiv = L.DomUtil.create('div', 'platform-dash');
                    _this.dash[platform.key] = ctldiv;
                    var $hdg = $("<input class='knob hdg' data-width='100' data-cursor='10' data-bgColor='#ffffff' data-fgcolor='#428bca' data-thickness='.3'  value='0' data-min='0' data-max='360'>").knob({readOnly: true});
                    var $rol = $("<input class='knob rol' data-width='66' data-cursor='157' data-angleOffset='0' data-bgColor='#cccccc' data-fgcolor='#326594' data-thickness='0.9'  value='0' data-min='-180' data-max='180'>").knob({readOnly: true});
                    var $alerts = $("<div style='display: inline; float: left; width:125px'><div style='font-weight: bold; font-size: 16px'><i class='fa fa-dot-circle-o platform-icon' style='color: " + bgcol + "'></i> " + platform.name + "</div></div>");
                    var $dials = $("<div style='display: inline; float: left; width:125px'></div>");

                    $alerts.append(
                        $("<div class='platform-alerts'></div>"),
                        $("<div class='platform-stat'></div>")
                    );
                    if (dispoptions.showbatt) {
                        $alerts.append(
                            $("<div>Battery:</div>"),
                            $("<div class='progress' style='width: 90%;'><div class='progress-bar bat' role='progressbar' aria-valuenow='1' aria-valuemin='0' aria-valuemax='100' style='width: 1%;'>0%</div></div>")
                        );
                    }
                    if (dispoptions.showrph) {
                        $dials.append(
                            $("<div class='hdg-rol'></div>").append(
                                $("<div class='rol-canvas'></div>").html($rol),
                                $("<div class='hdg-canvas'></div>").html($hdg),
                                $("<div class='rph-info'>H:<br>P:<br>R:</div>")
                            )
                        );
                    }
                    $dials.append($("<div class='platform-pose'></div>"));

                    $(ctldiv).append($alerts, $dials).width(270);

                    // add dashboard icon to info panel
                    $(_this.info[platform.key]).parent().find('.pbtns').append(
                        $("<i class='fa fa-dashboard platform-ctrl active' id='dash-" + platform.key + "'></i>")
                            .click(function () {
                                $(_this.dash[platform.key]).toggle();
                                if ($(_this.dash[platform.key]).is(":visible")) $(this).addClass("active");
                                else $(this).removeClass("active");
                            })
                            .tooltip({title: "Show/hide dash", trigger: "hover", container: "body"})
                    );
                    return ctldiv;
                }
            });
            this.map.addControl(new ctl());
        }
    };

    /**
     * Add generic control to map.
     * @param html:     icon or html to insert into control
     * @param clickfnc: function to execute on click
     * @param title:    title to show on mouse over
     * @param position
     */
    this.add_control = function (html, clickfnc,title, position) {
        if (typeof position === 'undefined') position='topleft'; // set default position for control
        var tooltipos = (position.indexOf("right") > -1) ? "left" : "right"; // pick best tooltip location
        var thisctldiv;
        var ctl = L.Control.extend({
            options: {
                position: position
            },
            onAdd: function (map) {
                var ctldiv = L.DomUtil.create('div', 'map-control');
                $(ctldiv).html(html).click(clickfnc).tooltip({placement:tooltipos,container:"body",title:title,trigger:"hover"});
                thisctldiv = ctldiv;
                return ctldiv;
            }
        });
        this.map.addControl(new ctl());
        return thisctldiv;
    };


    this.add_layer = function (data, url, layercfg) {

        var _that = this;  // this is required to work around a strange context bug that occurs when calling it
        // _this... May have something to do with the fact that it is called as a function pointer in index.html

        // Load target layer
        // --------------------------------------------------------------------
        if (data.type == "layer-target") {
            var platform = {
                name: data.settings.name,
                key: layercfg.match(/([^\/]+)(?=\.\w+$)/)[0],
                commandurl: data.settings.commandurl,
                url: data.settings.url
            };
            _that.map.add_posetracker(platform, data.settings.url, {color: data.settings.color, interval: data.settings.interval, size: data.settings.size, maxtracklen: data.settings.maxtracklen}, data.settings.options);
            if (data.settings.options.autotrack) _that.map.auto_extent(platform.key, true);
            _this.targetlist.push(platform);
        }

        // Load mission layer
        // --------------------------------------------------------------------
        else if (data.type == "layer-mission") {
            // check is there is an origin specified in the cfg and pass it in if it does
            if (data.settings.hasOwnProperty('origin_lat') && data.settings.hasOwnProperty('origin_lon')) {
                var origin = [data.settings.origin_lat , data.settings.origin_lon];
                _that.map.get_mission(data.settings.name, data.settings.filepath, data.settings.url, {interval: data.settings.interval}, origin);
            }
            else
                _that.map.get_mission(data.settings.name, data.settings.filepath, data.settings.url, {interval: data.settings.interval, clearold: data.settings.clearold});
        }

        // Load tile layer
        // --------------------------------------------------------------------
        else if (data.type == "layer-tile") {
            if (data.settings.type === "overlay") {
                var opacity = (data.settings.hasOwnProperty("opacity")) ? data.settings.opacity : 1;
                _that.map.layers.overlays[data.settings.name] = new L.TileLayer(data.settings.url, {maxNativeZoom: data.settings.maxzoom, maxZoom: _that.map.map.maxZoom, opacity: opacity});
                _that.map.layerctl.addOverlay(_that.map.layers.overlays[data.settings.name], data.settings.name);
                if (data.settings.addtomap)_that.map.layers.overlays[data.settings.name].addTo(_that.map.map);
            }
            else if (data.settings.type === "tms-overlay") {
                var opacity = (data.settings.hasOwnProperty("opacity")) ? data.settings.opacity : 1;
                _that.map.layers.overlays[data.settings.name] = new L.TileLayer(data.settings.url, {tms:true, maxNativeZoom: data.settings.maxzoom, maxZoom: _that.map.map.maxZoom, opacity: opacity});
                _that.map.layerctl.addOverlay(_that.map.layers.overlays[data.settings.name], data.settings.name);
                if (data.settings.addtomap)_that.map.layers.overlays[data.settings.name].addTo(_that.map.map);
            }
            else {
                if (data.settings.type === "baselayer") {
                    var attribution = (data.settings.hasOwnProperty('attribution')) ? data.settings.attribution + " | " : "";
                    attribution += "Made by <a href='http://greybits.com.au' target='_blank'>Greybits Engineering</a>";
                    _that.map.layers.base[data.settings.name] = new L.TileLayer(data.settings.url, {maxNativeZoom: data.settings.maxzoom, maxZoom: _that.map.map.maxZoom, attribution: attribution});
                    _that.map.layerctl.addBaseLayer(_that.map.layers.base[data.settings.name], data.settings.name);
                }
                else if (data.settings.type === "tms-baselayer") {
                    var attribution = (data.settings.hasOwnProperty('attribution')) ? data.settings.attribution + " | " : "";
                    attribution += "Made by <a href='http://greybits.com.au' target='_blank'>Greybits Engineering</a>";
                    _that.map.layers.base[data.settings.name] = new L.TileLayer(data.settings.url, {tms:true, maxNativeZoom: data.settings.maxzoom, maxZoom: _that.map.map.maxZoom, attribution: attribution});
                    _that.map.layerctl.addBaseLayer(_that.map.layers.base[data.settings.name], data.settings.name);
                }
                else if (data.settings.type === "google-baselayer") {
                    _that.map.layers.base[data.settings.name] = new L.Google(data.settings.url);
                    _that.map.layerctl.addBaseLayer(_that.map.layers.base[data.settings.name], data.settings.name);
                }
                // layer to map as default baselayer
                if (data.settings.addtomap) {
                    _that.map.map.addLayer(_that.map.layers.base[data.settings.name]);
                    _that.baselayeradded = true;
                }
            }
        }

        // Load heatmap layer
        // --------------------------------------------------------------------
        else if (data.type == "layer-heatmap") {
            //console.log(data);
            _that.map.get_heatmap(data);
        }



        // Load kml layer
        // --------------------------------------------------------------------
        else if (data.type == "layer-kml") {
            _that.map.get_kml(data.settings.name, data.settings.filepath);
        }

        // Load widgets
        // --------------------------------------------------------------------
        else if (data.type == "widget") {
            var $iframectr = [];
            var $iframe;
            var obj = _that.map.add_control("<i class='fa fa-"+data.settings.icon+"'></i>", function(e){
                //var $iframe = $("<iframe style='width:100%; border:none;' src='" + data.url + "'></iframe>");
                if (data.settings.type == "iframe-draggable" || data.settings.type == "draggable") {
                    if($iframectr.length > 0 && $(this).hasClass("active")) {
                        $iframectr.remove(); // destroy on hide
                        $(this).removeClass("active");
                    }
                    else {
                        $iframectr = $("<div class='iframe' style='" + data.settings.css + "'>").append(
                            $("<div class=iframe-title><i class='fa fa-" + data.settings.icon + "'></i> " + data.settings.name + "</div>").append(
                                $("<i class='iframe-close fa fa-times pull-right'></i>").click(function(){$iframectr.data("ctl").trigger("click");})
                            )
                        ).data("ctl",$(this));
                        $("body").append($iframectr);
                        if (data.settings.type == "iframe-draggable")
                            $iframe = $("<iframe style='height:"+($iframectr.height() - 30)+"px;' src='" + data.settings.url + "'></iframe>");
                        else
                            $iframe = $("<div>").load(data.settings.url);
                        $iframectr.append($iframe);
                        $iframectr.draggable({containment: "#map", scroll: false});
                        $(this).addClass("active");
                    }
                }
                else if (data.settings.type == "iframe-modal") {
                    // TODO: remove dependancy on external show_modalform function
                    show_modalform (data.settings.url, true);
                }
                else if (data.settings.type == "modal") {
                    // TODO: remove dependancy on external show_modalform function
                    show_modalform (data.settings.url);
                }
            },data.settings.name,"topright");
            // if set to show, then trigger click
            if (data.settings.show == true) {
                $(obj).trigger("click");
            }
        }

        // Load geotiff layer
        // --------------------------------------------------------------------
        else if (data.type == "layer-geotiff") {
            _that.map.get_geotiff(data.settings.name, data.settings.filepath, "/get_geotiff");
        }
    };


//    /**
//     *
//     * @param platform
//     */
//    function setwaypoint (platform) {
//        //console.log("!!!!!!!!!!!!!!!",platform)
//        platform_modal("static/html-snippets/form-target-command.html", platform.urlparam);
//    }
//
//    function platform_modal (htmlurl, platform) {
//        $("#modalcontent").load(htmlurl,function(){
//            $("#auvstatetitle").html(platform);
//            $("#auvstateplatform").val(platform);
//            $("#auvcmdplatform").val(platform);
//            $("#modal").modal("show");
//        })
//    }


    /**
     * Zoom to extent of track for a given platform.
     * Toggles tracking if 'forceautotrack' is not specified
     * Cancels any other existing tracker
     * @param platform
     * @param forceautotrack: true = enable autotrack, false = disable
     */
    this.auto_extent = function (platform, forceautotrack) {
        var autotrack =  (typeof forceautotrack === "undefined") ? ! _this.snap_to_track[platform] : forceautotrack;

        if (platform == "all" && autotrack == false) platform = Object.keys(_this.snap_to_track);
        else platform = [platform];

        $(platform).each(function(i,p) {
            _this.snap_to_track[p] = autotrack;
            if (autotrack) {
                $("#snap-" + p.replace(" ","_")).addClass("active");
                _this.autotrack_layer.addLayer(_this.layers.overlays[p]);
                //console.log(p, _this.layers.overlays)
            }
            else {
                $("#snap-" + p.replace(" ","_")).removeClass("active");
                _this.autotrack_layer.removeLayer(_this.layers.overlays[p]);
                _this.map.addLayer(_this.layers.overlays[p]);
            }
        });
    };


    function round(num, dp) {
        return Math.round(num * Math.pow(10, dp)) / Math.pow(10,dp);
    }

    /*
     * Helper functions for drawing the ship onto the map
     */

    function getShipPoly (size, shiplayer) {
        // Ship polygon is represented as a set of points in meters
        var length = size.length, width=size.width, offset=size.loffset;
        this.points = [[(-width / 2), 0.0-offset],
            [(width / 2), 0.0-offset],
            [(width / 2), (length - width-offset)],
            [0.0, (length-offset)],
            [(-width / 2), (length - width-offset)]
        ];
        this.shiplayer = shiplayer;

        function mdeglat(lat) {
            var latrad = lat/180*Math.PI;
            return 111132.09 - 566.05 * Math.cos(2.0*latrad) + 1.20 * Math.cos(4.0*latrad) - 0.002 * Math.cos(6.0*latrad) ;
        }

        function mdeglon(lat) {
            var latrad = lat/180*Math.PI;
            return 111415.13 * Math.cos(latrad) - 94.55 * Math.cos(3.0*latrad)  + 0.12 * Math.cos(5.0*latrad);
        }

        this.setLatLngHdg = function (heading, position) {
            var hdg = (((heading-90)/180)*Math.PI);

            // Rotate the points in the points array
            // Convert to LL and shift to the correct location
            var point = [],
                pointsLL = [];
            for (var i=0; i<this.points.length; i++) {
                point = [
                    (this.points[i][0] * Math.cos(hdg) - this.points[i][1] * Math.sin(hdg)) ,
                    (this.points[i][0] * Math.sin(hdg) + this.points[i][1] * Math.cos(hdg))
                ];
                pointsLL[i] = new L.LatLng(0,0);
                pointsLL[i].lat = (point[0] / mdeglat(position.lat) + position.lat);
                pointsLL[i].lng = (point[1] / mdeglon(position.lat) + position.lng);
            }
            this.shiplayer.setLatLngs(pointsLL);
            return this.shiplayer;
        }
    }
}
