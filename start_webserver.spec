# -*- mode: python -*-

block_cipher = None

a = Analysis(
    ['start_webserver.py'],
    pathex=['/Users/ariell/Projects/themaptracker'],
    binaries=None,
    datas=[
        ('static', 'static'),
        ('default_config/servers/_default.json','default_config/servers/'),
        ('default_config/tilelayers/basemap-googlesat.json','default_config/tilelayers'),
        ('default_config/tilelayers/basemap-google.json','default_config/tilelayers'),
        ('default_config/tilelayers/basemap-watercolour.json','default_config/tilelayers'),
        ('default_config/tilelayers/basemap-esri-ocean.json','default_config/tilelayers'),
        ('default_config/tilelayers/basemap-osm.json','default_config/tilelayers'),
        ('default_config/targets/simplehttp-mydevice1.json','default_config/targets'),
        ('default_config/targets/api-test.json','config/targets'),
        ('default_config/missions/api-test_mission.json','default_config/missions'),
        ('default_config/widgets/video.json','config/widgets'),
        ('default_config/widgets/post-note-social-media.json','default_config/widgets'),
        ('default_config/widgets/simplehttp-post-mydevice.json','default_config/widgets'),
        ('default_config/widgets/logconsole-api.json','default_config/widgets'),
        ('default_config/heatmaps/demo-sensor-heatmap.json','default_config/heatmaps'),
        ('default_config/heatmaps/demo-sensor-heatmap-scaled.json','default_config/heatmaps'),
        ('platformdata/__init__.py','platformdata'),
        ('platformdata/PlatformBase.py','platformdata'),
        ('platformdata/simplehttp.py','platformdata'),
        ('platformdata/api.py','platformdata'),
        ('platformdata/demo.py','platformdata'),
        ('datatools/__init__.py','datatools'),
        ('datatools/mbtiles.py','datatools'),
        ('datatools/geotiff.py','datatools'),
        ('templates/maptracker.html','templates'),
        ('templates/html-snippets/form-licensekey.html','templates/html-snippets'),
        ('templates/html-snippets/modal-settings.html','templates/html-snippets'),
        ('templates/html-snippets/form-config-json.html','templates/html-snippets'),
        ('templates/html-snippets/form-mydata.html','templates/html-snippets'),
        ('templates/platforms/send-command-demo.html','templates/platforms'),
        ('templates/platforms/set-state.html','templates/platforms'),
        ('templates/plugins/cache_tiles.html','templates/plugins'),
        ('templates/plugins/geotiff_click_info.html','templates/plugins'),
        ('templates/html-snippets/post-note-social-media.html','templates/html-snippets'),
        ('templates/iframes/simplehttp-post.html','templates/iframes'),
        ('templates/iframes/logconsole.html','templates/iframes'),
        ('external_scripts/demo_api_post.py','external_scripts'),
        ('external_scripts/demo_fake_data.py','external_scripts'),
        ('uploads/README.txt','uploads'),
        ('version','.')
    ],
    hiddenimports=['geojson', 'sqlite3', 'landez', 'wget', 'matplotlib', 'LatLon', 'numpy'],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher
)

pyz = PYZ(
    a.pure,
    a.zipped_data,
    cipher=block_cipher
)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    name='start_webserver',
    debug=False,
    strip=False,
    upx=True,
    console=True ,
    icon='maptracker.ico'
)
