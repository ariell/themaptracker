#!/usr/bin/python

######################################################################
# Simple webserver that runs mission viewer.
#
# REQUIREMENTS: to run this code make sure you have the following
# dependencies:
#   sudo pip install -r requirements.txt
#
# HOW TO RUN:
#   python start_webserver.py
#
# HOW TO TEST:
# If you have the UI code, open a web browser and go to:
#   localhost:8080/
# If you do not have the UI code, you can still test that the  platform
# data threads are running properly, open a web browser and go to:
#   localhost:8080/get_data?platform=0
# where "0" is the ID of the platform you would like to get data for.
# You should see a JSON message containing the data structure that
# should update on refresh.
#
# NOTE:
# If you would like to add your platform to the mission viewer, you
# have come to the right place, but you're in the wrong file.
# For anything basic, you shouldn't need to edit this file. The file
# you are looking for is:
#   platformdata_thread.py
#
# That file does all the heavy lifting for platform pose updates.There
# should be enough details in there to help. If it is not obvious please
# contact me.
#
# Author:
#   Ariell Friedman
#   ariell.friedman@gmail.com
#   25 AUG 2014
#
######################################################################
import base64

# import datetime
from collections import OrderedDict

from flask import Flask, Response, request, render_template, url_for, send_from_directory, send_file, session   #, abort, redirect, jsonify
from flask_jsonpify import jsonify  # allow cross-domain jsonp messages (for use across the network)
from werkzeug import secure_filename
import socket  # to get local hostname and ip address easily
from osgeo import gdal      # for geotiff reading
from PIL import Image       # for geotif conversion
import os
# import sys
# from gevent.wsgi import WSGIServer    # host it properly
import json
import glob
from shutil import copyfile, copytree

from maptracker_utils import create_app, check_license, get_json, get_module, upload_zip, download_zip, reset_default_files
import argparse

#import inspect

# import datatools
# import requests
# import threading
# import ConfigParser
# import shutil


settings = {
    "port": 8080,
    "ipaddress": "0.0.0.0",
    "hostname": socket.gethostname(),
    "staticdir": "static",
    "application_path": os.path.dirname(os.path.realpath(__file__)),
    "rootdir": os.path.dirname(os.path.realpath(__file__)),
    "template_folder": "templates",
    "cfgbasedir": "config",
    "servercfg_pattern": os.path.join("servers", "{}.json"),
    "servercfg": None,
    "version": 0,
    "license_params": {},
    "offline_mode": False
}

# Parse input args
parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", default=8080, type=int, help="Port to maptracker server")
parser.add_argument('--offline', dest='offline', action='store_true', help="Run in offline mode")
parser.set_defaults(offline=False)
args = parser.parse_args()

# Modify settings
settings["port"] = args.port
settings["offline_mode"] = args.offline

# Create app
authenticated = True
app = create_app(settings)
app.debug = True
# Session
app.secret_key = os.urandom(24)
# app.config['SESSION_TYPE'] = 'filesystem'

CFG_CAMPAIGN = get_json(os.path.join(settings["cfgbasedir"], settings["servercfg"]), 'campaign')

print (CFG_CAMPAIGN, settings)

@app.route('/', defaults={"replay_campaign": CFG_CAMPAIGN})
@app.route("/archive", defaults={"replay_campaign": None})
@app.route("/archive/<replay_campaign>")
def home(replay_campaign):
    global settings
    parameters = request.args.to_dict()
    if replay_campaign is None:
        return render_template("archive.html")
    session["replay_campaign"] = replay_campaign   # set session variable for campaign
    license_key = get_json(os.path.join(settings["cfgbasedir"], settings["servercfg"]), 'license')
    #license_is_valid, license_params = check_license(license_key)
    license_is_valid = True
    #print "\nLicense information:\n  Expiration: {} ({})".format(license_params["expiredate"], license_params["daysleft"])
    return render_template('maptracker.html', configfile=settings["servercfg"], license_is_valid=license_is_valid, parameters=parameters, offline=settings["offline_mode"])
    #return render_template('index.html', server="http://localhost:8080")  # server is this machine


@app.route('/campaign', defaults={"campaign": "*"})
@app.route('/campaign/<campaign>')
def archive(campaign):
    # TODO: add log dir to config file
    logdir = "platformdata/logs"
    values = OrderedDict([
        ("campaign", campaign),
        ("year", "*"),
        ("month", "*"),
        ("day", "*")
    ])
    paths = {
        "campaign": logdir+"/{values[campaign]}",
        "year": logdir+"/{values[campaign]}/archive/platform/{values[year]}",
        "month": logdir+"/{values[campaign]}/archive/platform/{values[year]}/{values[month]}",
        "day": logdir+"/{values[campaign]}/archive/platform/{values[year]}/{values[month]}/{values[day]}"
    }
    data = {"campaigns": []}
    get_listing(data["campaigns"], paths, values, 0)
    return jsonify(data)


def get_listing(data, paths, values, ind):
    fields = values.keys()
    field = fields[ind]
    path = paths[field]
    local_values = values.copy()
    ind = ind+1
    for p in glob.glob(path.format(values=local_values)):
        f = os.path.basename(p)
        local_values[field] = f
        if ind < len(fields):
            d = {field: f, fields[ind]+"s": []}
            get_listing(d[fields[ind]+"s"], paths, local_values, ind)
        else:
            d = f
        data.append(d)
    return data


@app.route('/get_mission/<module>/<platform>')
def get_mission(module, platform):
    filepath = request.args.get('filepath')  # mission file path
    if request.args.get('olat') and request.args.get('olon'):
        origin = [request.args.get('olat'), request.args.get('olon')]
    else:
        origin = None

    replay_campaign = session.get("replay_campaign", CFG_CAMPAIGN)
    return jsonify(get_module(module).parse_mission(platform, filepath, origin, campaign=replay_campaign))
    #latlngs, origin = get_module(module).parse_mission(filepath, [olat, olon])
    #return jsonify({"latlngs": latlngs, "origin": origin})


@app.route('/get_geotiff')
def get_geotiff():
    # This function needs to get a url for a geotiff, compute the bounds and return the rquired variables as json
    url = request.args.get('url')
    chartdirurl = 'uploads/geotiffs/'

    ds = gdal.Open(url)
    gt = ds.GetGeoTransform()
    bounds = [
        [gt[3], gt[0]],
        [gt[3] + ds.RasterXSize*gt[4] + ds.RasterYSize*gt[5], gt[0] + ds.RasterXSize*gt[1] + ds.RasterYSize*gt[2]]
    ]

    # Leaflet cannot display geotiffs in the browser, so we need to convert them to pngs
    imgurl = chartdirurl+os.path.basename(url)+'.png'
    if not os.path.isfile(imgurl):
        Image.open(url).save(imgurl)

    return jsonify({'bounds': bounds, 'imgurl': imgurl})


# TODO: rename to run_datatool to plugin
@app.route('/run_datatool/<module>', methods=['POST', 'GET'])  # for backwards compatibility
@app.route('/plugin/<module>', methods=['POST', 'GET'])
def plugin(module):
    """
    Runs a standalone function directly
    """
    # Handle POST request for running plugin module
    mod = get_module(module, execute=False)  # get plugin module, but don't execute
    if request.method == 'POST':  # expects JSON data and content type
        if 'application/json;' in request.headers['Content-Type']:
            options = request.get_json()
            resp = mod(**options)                      # execute with options from params
            return jsonify(resp)
        else:
            return jsonify({'response': 'danger', 'msg': 'Content-Type must be "application/json"'})
    elif request.method =='GET':
        options = request.args.to_dict()
        resp = mod(**options)                      # execute with options from params
        return jsonify(resp)
    else:
        return jsonify({'response': 'danger', 'msg': 'Expected POST with Content-Type "application/json"'})


@app.route('/run_method/<module>/<method>', methods=['GET'])  # for backwards compatibility
@app.route('/data-method/<module>/<method>', methods=['GET'])
def run_module_method(module, method):
    """
    Runs a generic method of module (or class)
    """
    options = request.args.to_dict()
    data = getattr(get_module(module), method)(**options)
    return jsonify(data)


@app.route('/get_data/<module>/<platform>', methods=['GET', 'POST'])  # for backwards compatibility
@app.route('/data/<module>/<platform>', methods=['GET', 'POST'])
def platformdata(module, platform):
    global CFG_CAMPAIGN
    platform_module = get_module(module)

    # Handle POST request for setting data
    if request.method == 'POST':  # expects JSON data and content type
        options = request.args.to_dict()
        data = request.get_json() if 'application/json' in request.headers['Content-Type'] else dict(request.form)
        try:
            platform_module.set_data(platform, data, campaign=CFG_CAMPAIGN, **options)
        except ValueError as e:
            return jsonify({'response': 'error', 'msg': '{}'.format(e)}), 400
        return jsonify({'response': 'success', 'msg': 'Data was updated successfully!'})

    # Otherwise get data for platform
    else:
        gethistory = request.args.get('gethistory') == "true"

        # Render html form for setting state
        if request.args.get('getform') == "true":
            return render_template('platforms/set-state.html', platform=platform, module=module)

        replay_ts = request.args.get("ts", None)
        replay_campaign = request.args.get("campaign", session.get("replay_campaign", CFG_CAMPAIGN))   # check if session campaign is set, otherwise just use global
        data = platform_module.get_data(platform, gethistory=gethistory, campaign=replay_campaign, replay_ts=replay_ts)

        # check for no data
        if not data:
            return jsonify({'message': 'No data found for: {}'.format(platform)}), 404

        # deal with setting state of vehicle
        if 'state' in data:
            if data['state'] == 'follow' or data['state'] == 'stationary':
                data['msgts'] = data['curts']-1     # dirty hack to force update on state change
            elif (data['curts']-data['msgts']) > 2:  # if old message show uncertainty
                # TODO: fix hard coded speed
                speed = data['pose']['speed'] if 'speed' in data['pose'] else 0.5
                data['pose']['uncertainty'] = (data['curts']-data['msgts']) * speed

    return jsonify(data)


# Custom static data
@app.route('/uploads/<path:filename>')
def uploads(filename):
    return send_from_directory(os.path.join(settings["application_path"], 'uploads'), filename)

@app.route('/templates/<path:templatefile>')
def snippet(templatefile):
    parameters = request.args.to_dict()
    return render_template(templatefile, settings=settings, parameters=parameters)


@app.route('/get_config/<cfgdir>/<cfg>', methods=['GET', 'POST'])
@app.route('/get_config', defaults={'cfg': None, 'cfgdir': None}, methods=['GET'])
def get_config(cfgdir, cfg):
    global settings
    #cfgdirs = os.listdir(settings["cfgbasedir"])
    cfgdirs = ['geotiffs', 'kmls', 'missions', 'targets', 'tilelayers', 'widgets', 'heatmaps']
    servercfgfile = os.path.join(settings["cfgbasedir"], settings["servercfg"])
    cfgpath = os.path.join(secure_filename(cfgdir), secure_filename(cfg))
    cfgfile = os.path.join(settings["cfgbasedir"], cfgpath)

    # POST - save / overwrite config
    if request.method == 'POST':
        # TODO: check if login is required
        cfgtext = request.form.get('cfgtxt')
        cfgfield = request.form.get('cfgfield', None)
        if authenticated:
            if cfgfield is not None:
                cfgold = get_json(servercfgfile)
                cfgold[cfgfield] = cfgtext
                cfgtext = json.dumps(cfgold, indent=4)
            open(cfgfile, 'w').write(cfgtext)
            if cfgpath in get_json(servercfgfile, 'layers'):    # if the config is currently loaded,
                os.utime(servercfgfile, None)                   # touch server cfg to trigger update
            return jsonify({'msg': 'Config file was updated!'})
        return "You are not authorised to edit this config file!", 403
        #return jsonify({'response': 'error', 'msg': 'You do not have the authorisation!'}), 403

    # GET - check if action, otherwise return config
    else:
        action = request.args.get('action')
        if action == "getmtime":
            return jsonify({'mtime': os.path.getmtime(cfgfile)})
        elif action == "edit":
            cfgform = request.args.get('form') if request.args.get('form') is not None else 'html-snippets/form-config-json.html'
            cfgtype = get_json(cfgfile, 'type') if cfg is not None else None
            layer_list = []
            if cfgtype == "server":
                for cd in cfgdirs:
                    if os.path.isdir(os.path.join(settings["cfgbasedir"], cd)):
                        layer_list += glob.glob(os.path.join(settings["cfgbasedir"], cd, '*.json'))
            else:
                layer_list = get_json(servercfgfile, 'layers') if os.path.isfile(servercfgfile) else []
            # Get dict output of layer and layer types
            # TODO: this is a hacky way to get cfg listing without base dir in path
            layers = [{'cfgdir':c[-2], 'cfg': c[-1]} for c in [cn.split('/') for cn in layer_list]]

            return render_template(cfgform, cfgdir=cfgdir, cfg=cfg, servercfg=settings["servercfg"], layers=layers, type=cfgtype, cfgdirs=cfgdirs)
        elif action == "copy":
            try:
                cfg_rename = request.args.get('rename')
                copyfile(cfgfile, os.path.join(settings["cfgbasedir"], cfgdir, cfg_rename))
                return jsonify({'status': 'success', 'msg': cfgdir+'/'+cfg_rename+' was created successfully!', 'cfg': cfg_rename, 'cfgdir': cfgdir})
            except Exception:
                return jsonify({'status': 'dange', 'msg': 'ERROR! There was a problem creating: '+cfgdir+'/'+cfg_rename+'!', 'cfg': cfg_rename, 'cfgdir': cfgdir})
        else:
            return send_file(cfgfile)



@app.route('/add_comment', methods=["POST"])
def add_comment():
    args = dict(request.form)
    return jsonify(args)


@app.route("/mydata/<action>")
def mydata(action):
    global settings
    filelist = ["config/", "datatools/", "platformdata/", "templates/", "external_scripts/"]  #, "uploads/"]
    license_key = get_json(os.path.join(settings["cfgbasedir"], settings["servercfg"]), 'license')
    with open(os.path.join(settings["rootdir"], 'version')) as f:
        version = float(f.read())
    filename = "userdata/{}.v{}.zip".format(base64.urlsafe_b64encode(license_key), version)
    if not license_key or not version:
        response = {"success": False, "message": "Invalid licence or version file..."}
    elif action == "download":
        response = download_zip("http://greybits.com.au/maptracker/{}".format(filename), settings["application_path"])
    elif action == "upload":
        response = upload_zip("http://greybits.com.au/maptracker/upload.php", filename, filelist)
    elif action == "reset":
        response = reset_default_files(settings["rootdir"], settings["application_path"], filelist)
    else:
        response = {"success": False, "message": "Unrecognised action..."}
    return jsonify(response)


@app.route('/send_command/<module>/<platform>', methods=["POST", "GET"])
def send_command(module, platform):
    if request.method == 'POST':
        return get_module(module).send_command(platform, request.get_json())
    elif request.method == 'GET':
        return get_module(module).send_command(platform, request.args.to_dict(), getform=True)


@app.route("/me")
def find_my_friends ():
    return render_template("iframes/find-my-friends-post.html")


if __name__ == '__main__':
    print "Starting webserver in '{}'...".format(settings["application_path"])
    print "Static path: ", settings["staticdir"]
    print "To connect to this server from another machine on the network, open a browser and go to: \n\n" \
          "    http://{}:{}\n".format(settings["ipaddress"], settings["port"])

    #http_server = WSGIServer(('', settings["port"]), app, keyfile='ssl/key.key', certfile='ssl/key.crt')
    app.run(host="0.0.0.0", port=settings["port"], threaded=True)
    # http_server = WSGIServer(('', settings["port"]), app)
    # http_server.serve_forever()










