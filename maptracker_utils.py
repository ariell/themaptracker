import os
import sys
import json
from shutil import copytree, copyfile
import socket
import datetime

import requests
from flask import Flask
from Crypto.Cipher import AES
import base64
import urllib
import wget
import stat
from distutils import sysconfig as sc

from io import BytesIO
from zipfile import ZipFile
import base64


imported_modules = {}

# todo: this should be stored somewhere else safe/encrypted
license_params = {
    'date_format': '%Y%m%d',
    'cipher': AES.new('1234567890123456', AES.MODE_ECB)
}


def create_app(settings):
    # automatically work out IP address
    try:
        settings["ipaddress"] = (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["localhost"])[0]
    #     settings["ipaddress"] = socket.gethostbyname(settings["hostname"])
    except Exception:
        settings["ipaddress"] = settings["hostname"]

        #

    settings["template_folder"] = os.path.join(settings["application_path"], 'templates')
    settings["cfgbasedir"] = os.path.join(settings["application_path"], "config")
    settings["servercfg"] = settings["servercfg_pattern"].format(settings["hostname"])

    # If script is running as a stand alone executable bundle, check required files exist, otherwise copy them in
    if getattr(sys, 'frozen', False):
        settings["application_path"] = os.path.dirname(sys.executable)
        # add to python path
        sys.path.insert(0, settings["application_path"])    # ensure that modules in local dir are working
        sys.path.append(sc.get_python_lib(prefix='/usr/local/', plat_specific=True))           # add system python path
        sys.path.append(sc.get_python_lib(prefix='/usr/', plat_specific=True))           # add system python path

        settings["rootdir"] = sys._MEIPASS                  # local tmp created root dir
        settings["staticdir"] = os.path.join(settings["rootdir"], 'static')   # reference static files in temp directory that is created by pyinstaller

        # update current version if not running in offline mode
        if not settings["offline_mode"]:
            check_update(os.path.join(settings["rootdir"], 'version'), sys.executable)

        # Initialise default directories and files if first run
        sysfiles = ['config', 'platformdata', 'templates', 'uploads', 'datatools', 'external_scripts']
        for sf in sysfiles:
            if not os.path.isdir(os.path.join(settings["application_path"], sf)):
                print "Initialising {} directory...".format(sf)
                copytree(os.path.join(sys._MEIPASS, sf), os.path.join(settings["application_path"], sf))
    else:
        if not os.path.isdir(os.path.join(settings["application_path"], "config")):
            copytree(os.path.join(settings["application_path"], "default_config"), os.path.join(settings["application_path"], "config"))

    # elif __file__:
    #     settings["staticdir"] = "static"
    #     settings["application_path"] = os.path.dirname(os.path.realpath(__file__))

    # create server config if it does not already exist
    if not os.path.isfile(os.path.join(settings["cfgbasedir"], settings["servercfg"])):
        if not os.path.isdir(os.path.dirname(os.path.join(settings["cfgbasedir"], settings["servercfg"]))):
            os.makedirs(os.path.dirname(os.path.join(settings["cfgbasedir"], settings["servercfg"])))
        copyfile(os.path.join(settings["rootdir"], "default_config", "servers", "_default.json"), os.path.join(settings["cfgbasedir"], settings["servercfg"]))

    # Create
    app = Flask(__name__, template_folder=settings["template_folder"], static_folder=settings["staticdir"])

    return app


def check_license(license_key):
    try:
        today = datetime.datetime.now()
        email, datestr = license_params['cipher'].decrypt(base64.b64decode(license_key)).strip().split("|")
        license_date = datetime.datetime.strptime(datestr, license_params['date_format'])
        is_valid = license_date > today
        return is_valid, {"email":email, "expiredate":license_date.strftime("%Y-%m-%d"), "daysleft": (license_date-today).days, "key": license_key}
    except Exception as e:
        return False, e


def get_json(jsonfile, field=None):
    with open(jsonfile) as config_json:
        if field is None:
            return json.load(config_json)
        else:
            return json.load(config_json).get(field, None)


# get SW module (load if not already loaded)
def get_module(module, execute=True):
    global imported_modules
    if module not in imported_modules:
        components = module.split('.')
        mod = __import__(".".join(components[0:2]))
        for comp in components[1:]:
            mod = getattr(mod, comp)
        imported_modules[module] = mod() if execute else mod  # execute, otherwise return function pointer
    return imported_modules[module]

    # if module not in imported_modules:
    #     imported_modules[module] = getattr(__import__(module), module.split('.')[1])
    #     imported_modules[module].init()
    # return imported_modules[module]


def check_platform():
    if sys.platform.lower().startswith("linux"):
        return "linux"
    elif sys.platform.lower().startswith("darwin"):
        return "osx"
    elif sys.platform.lower().startswith("win"):
        return "windows"


def check_update(version_file, executable_path):
    update_url = "http://greybits.com.au/maptracker/current_version.json"
    archive_dir = os.path.join(os.path.dirname(executable_path), ".old")

    print "Checking for new version..."
    platform = check_platform()
    version = 0.0
    try:
        remote_version_info = json.load(urllib.urlopen(update_url))
        if os.path.isfile(version_file):
            with open(version_file) as f:
                version = float(f.read())
        if version < remote_version_info['version']:
            download_link = remote_version_info['platform'][platform]
            print "\nTHERE IS A NEW VERSION AVAILABLE!!!\n\n  Current version: {}\n  New version: {}\n".format(version, remote_version_info['version'])
            response = raw_input('Would you like to update now? [type: "YES", default is no] > ')
            if response.lower() == "yes":
                print "Downloading:", download_link, "..."
                wget.download(download_link, executable_path+"_new")
                print "\nReplacing:", executable_path, "..."
                if not os.path.isdir(archive_dir):
                    os.makedirs(archive_dir)
                os.rename(executable_path, os.path.join(archive_dir, "{}".format(version)))
                os.rename(executable_path+"_new", executable_path)
                os.chmod(executable_path, os.stat(os.path.join(archive_dir, "{}".format(version))).st_mode)  # make executable
                print "\nMaptracker has been updated successfully. Restart server."
                sys.exit()
        else:
            print "Running most recent version: {}".format(version)
    except Exception as e:
        print "\n\nERROR: Auto-update could not be completed.\n"
        print e


def download_zip(zipurl, savedir):
    #zipurl = 'http://greybits.com.au/maptracker/userdata/test.zip'
    try:
        print "Downloading and extracting to: ", savedir
        zipresp = urllib.urlopen(zipurl)
        with ZipFile(BytesIO(zipresp.read())) as zfile:
            zfile.extractall(savedir)
        return True
    except Exception as e:
        return False


def upload_zip(posturl, filename, filelist):
    try:
        myzipfile = BytesIO()
        myzip = ZipFile(myzipfile, 'w')
        for path in filelist:
            for root, dirs, files in os.walk(path):
                for f in files:
                    myzip.write(os.path.join(root, f))
        myzip.close()
        myzipfile.seek(0)
        post_response = requests.post(posturl, files={'file': (os.path.basename(filename), myzipfile.read())}, data={"path": os.path.dirname(filename)})
        return json.loads(post_response.content)
    except Exception as e:
        return {"success": False, "message": "ERROR: Something went wrong..."}


def reset_default_files(origin, destination, filelist):
    try:
        for path in filelist:
            for root, dirs, files in os.walk(os.path.join(origin,path)):
                for f in files:
                    sf = os.path.join(root, f)
                    df = sf.replace(origin, destination)
                    if not os.path.isdir(os.path.dirname(df)):
                        os.makedirs(os.path.dirname(df))
                    print "Copying {} to {}".format(sf, df)
                    copyfile(sf, df)
        return {"success": True, "message": "SUCCESS! Default files have been overwritten."}
    except Exception as e:
        return {"success": False, "message": "ERROR: Something went wrong..."}